// This is a test harness for your module
// You should do something interesting in this harness 
// to test out the module and to provide instructions 
// to users on how to use it by example.


// open a single window
var win = Ti.UI.createWindow({
	backgroundColor:'white'
});
var label = Ti.UI.createLabel();
win.add(label);
win.open();

// TODO: write your module tests here
var loginhash = require('cat.itinerarium.loginhash');
Ti.API.info("module is => " + loginhash);

label.text = loginhash.example();

Ti.API.info("module exampleProp is => " + loginhash.exampleProp);
loginhash.exampleProp = "This is a test value";

if (Ti.Platform.name == "android") {
	var proxy = loginhash.createExample({
		message: "Creating an example Proxy",
		backgroundColor: "red",
		width: 100,
		height: 100,
		top: 100,
		left: 150
	});

	proxy.printMessage("Hello world!");
	proxy.message = "Hi world!.  It's me again.";
	proxy.printMessage("Hello world!");
	win.add(proxy);
}

var saltHttpPetition = Ti.Network.createHTTPClient({
	onload : function(e) {
		
		var responseSalt = JSON.parse(this.responseText);
		generateSaltedPassword(responseSalt.salt);
	},
	onerror : function(e) {
		alert('error: ' + e.error);
	}
});
saltHttpPetition.open('POST', "http://dev.eduloc.net/api/getsalt");
saltHttpPetition.send({
	'_username' : 'pvives'
});

function generateSaltedPassword(salt){
	var password = 'provaprova';
	/*
	var combinationString = username + '{' + salt + '}';
	//var processedSalt = sha512utils.hex_sha512(combinationString);
	var processedSalt = sha512utils.b64_sha512(combinationString);
	
	for(var i=1; i<5000; i++){
		processedSalt = sha512utils.b64_sha512(processedSalt + combinationString);
	}
	var base64processedSalt = Ti.Utils.base64encode(processedSalt);
	alert(base64processedSalt);
	requestSomeProtectedUrl(base64processedSalt);
	*/
	
	//var processedSalt = Ti.Utils.sha512iteration(username, salt, 5000);
	var androidModule = require('cat.itinerarium.loginhash');
	var processedSalt = androidModule.processSHA512(password, salt, 5000);
	
}

