// Consulto idioma de la app i carrego fitxers de traduccio
var llenguaGuardada = Titanium.App.Properties.getString("languageGuardada", "undefined");
var idioma;
var idiomaWeb;

var mapaMHMInstanceInicial;
if (llenguaGuardada == 'ca') {
	Titanium.App.Properties.setString("language", 'ca');
	Titanium.include('/controller/translations.js');
	idioma = 'ca';
	idiomaWeb = 'CA';
} else if (llenguaGuardada == 'es') {
	Titanium.App.Properties.setString("language", 'es');
	Titanium.include('/controller/translations_es.js');
	idioma = 'es';
	idiomaWeb = 'ES';
} else if (llenguaGuardada == 'en') {
	Titanium.App.Properties.setString("language", 'en');
	Titanium.include('/controller/translations_en.js');
	idioma = 'en';
	idiomaWeb = 'EN';
} else {
	// Si no sha definit cap idioma agafo per defecte anglès

	Titanium.App.Properties.setString("language", 'en');
	Titanium.include('/controller/translations_en.js');
	idioma = 'en';
	idiomaWeb = 'EN';
}
Ti.App.idioma = idioma;

// URLS

// var urlBase = 'http://mhm.mobileworldcapital.com';
// Ti.App.urlBase = 'http://mhm.mobileworldcapital.com';

var urlBase = 'http://km2ciutat.itinerarium.cat';
Ti.App.urlBase = 'http://km2ciutat.itinerarium.cat';

Ti.App.entorn = 'citizensqkm';
Ti.App.idEscenariMHM = '62';
var urlMHM = urlBase + '/api/objectes/' + Ti.App.idEscenariMHM + '/' + idioma + '_' + idiomaWeb + '/' + Ti.App.entorn;
Ti.App.colorPrincipal = '#c31d2e';
Ti.App.colorSecundari = '#c31d2e';
Ti.App.colorTerciari = 'white';
// var afegirPuntButton;
// var filtreButton;
Ti.API.info(urlBase + '/api/objectes/' + Ti.App.idEscenariMHM + '/' + idioma + '_' + idiomaWeb + '/' + Ti.App.entorn);
// Variable comprovacio Android
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}

// Variable comprovacio iOS7
function isiOS7Plus() {
	if (Titanium.Platform.name == 'iPhone OS') {
		var version = Titanium.Platform.version.split(".");
		var major = parseInt(version[0], 10);
		if (major >= 7) {
			return true;
		}
	}
	return false;
}

var iOS7 = isiOS7Plus();

// Variables size pantalla
var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;

var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;
var loggedIn = Titanium.App.Properties.getString("loggedIn", "false");

//tipografies

if (Ti.Platform.osname == 'android') {
	var customFont = 'gotham-rounded-medium';
	var customFontBarraSuperior = 'gotham-rounded-medium';
	var customFontBold = 'gotham-rounded-medium';
} else {
	var customFont = 'Apex New';
	var customFontBarraSuperior = 'Apex New';
	var customFontBold = 'Apex New';
}

//Calculo resolucio pantalla android per adaptar fontSizes i densitats de les imatges
Ti.App.paddingLabels = 0;

if (isAndroid) {
	if (heightpantalla < 470 * Ti.Platform.displayCaps.logicalDensityFactor) {
		Ti.App.paddingLabels = 0;

	} else if (heightpantalla >= 470 * Ti.Platform.displayCaps.logicalDensityFactor && heightpantalla < 640 * Ti.Platform.displayCaps.logicalDensityFactor) {
		Ti.App.paddingLabels = 0;

	} else if (heightpantalla >= 640 * Ti.Platform.displayCaps.logicalDensityFactor && heightpantalla < 960 * Ti.Platform.displayCaps.logicalDensityFactor) {

		Ti.App.paddingLabels = 3;
	} else if (heightpantalla >= 960 * Ti.Platform.displayCaps.logicalDensityFactor) {

		Ti.App.paddingLabels = 8;
	}
}

Ti.App.isTablet = (heightpantalla >= 960) && isAndroid ? true : false;

// Declaro vistes dels apartats de la app
var viewMHMInstance = null;
var tableViewProjectesInstance = null;
var fitxaSettingsInstance = null;
var fitxaIdiomaInstance = null;
var mapaMHMInstanceInicial = null;

var NappDrawerModule = require('dk.napp.drawer');
var drawer;

//finestra central, en android el napp drawer necessita una view i en iOS una window
if (!isAndroid) {
	var winCentral = Ti.UI.createWindow({
		navBarHidden : true,
		modal : false,
		backgroundColor : Ti.App.colorPrincipal,
		idEscenari : Ti.App.idEscenariMHM,
		urlBase : urlBase,

	});
} else {
	var winCentral = Ti.UI.createView({
		navBarHidden : true,
		modal : false,
		backgroundColor : 'white',
		idEscenari : Ti.App.idEscenariMHM,
		urlBase : urlBase
	});
}
Ti.App.barraSuperior = Titanium.UI.createView({
	backgroundColor : Ti.App.colorPrincipal,
	top : iOS7 ? 20 : 0 * formulaHeight,
	height : isAndroid ? 44 * formulaHeight : 44,
	width : 320 * formulaWidth,
	left : 0 * formulaWidth
});
winCentral.add(Ti.App.barraSuperior);

Ti.App.labelBarraSuperiorMapa = Ti.UI.createLabel({
	text : 'MANRESA',
	left : 44 * formulaWidth,
	textAlign : 'center',
	height : isAndroid ? 44 * formulaHeight : 44,
	width : 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
	color : Ti.App.colorTerciari,
	backgroundColor : 'transparent',
	top : 0 * formulaHeight,
	font : {
		fontSize : 12 + Ti.App.paddingLabels + 'sp',
		fontFamily : customFontBarraSuperior
	}
});
Ti.App.barraSuperior.add(Ti.App.labelBarraSuperiorMapa);

var menuButton = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
	left : 0 * formulaWidth,
	top : 0 * formulaHeight,
	backgroundImage : '/images/menu.png'

});

Ti.App.barraSuperior.add(menuButton);

if (Ti.App.isTablet) {
	menuButton.backgroundImage = '/imagesTablet/menu.png';
}
menuButton.addEventListener("click", function(e) {
	drawer.toggleLeftWindow();
});

var infoButton = Ti.UI.createButton({
	width : 44 * formulaWidth,
	height : isAndroid ? 44 * formulaHeight : 44,
	right : 0 * formulaWidth,
	top : 0 * formulaHeight,
	backgroundImage : '/images/info.png'

});

// Ti.App.barraSuperior.add(infoButton);

if (Ti.App.isTablet) {
	infoButton.backgroundImage = '/imagesTablet/info.png';
}

//finestra lateral, en android el napp drawer necessita una view i en iOS una window
if (!isAndroid) {
	var winLateral = Ti.UI.createWindow({
		navBarHidden : true,
		modal : false,
		backgroundColor : 'transparent',
		idEscenari : Ti.App.idEscenariMHM,
		urlBase : urlBase,
		// top:iOS7 ? 20 : 0 * formulaHeight
	});
} else {

	var winLateral = Ti.UI.createView({
		navBarHidden : true,
		modal : false,
		backgroundColor : 'transparent',
		idEscenari : Ti.App.idEscenariMHM,
		urlBase : urlBase
	});
}
winCentral.orientationModes = [Ti.UI.PORTRAIT];

//finestra carregant...
var downloaderWindow = require('/controller/offline/DownloaderWindow');
var downloaderWindowInstance = new downloaderWindow(carregant);

// Creo la finestra inicial: mapa MHM
// Crida HTTP per agafar les dades del escenari de MHM
var dataMHM = require('/controller/creacioEscenari/networkControllerInicial');
new dataMHM(urlMHM, callbackMapaMHM, downloaderWindowInstance);

function callbackMapaMHM(data) {

	// // Creo el mapa del MHM
// 
	var mapaMHM = require('/controller/creacioEscenari/mapController');
	mapaMHMInstanceInicial = new mapaMHM(winCentral, data, 'mapaInicial', Ti.App.idEscenariMHM);
	winCentral.add(mapaMHMInstanceInicial);

	// Llistat amb els diferents apartats de la app (finestra esquerra del Napp Drawer)
	var llistatDrawer = require('/controller/llistesEscenaris/llistatDrawer');
	var llistatDrawerInstance = new llistatDrawer(urlBase, mapaMHMInstanceInicial, winCentral);
	winLateral.add(llistatDrawerInstance);

	if (!isAndroid) {
		drawer = NappDrawerModule.createDrawer({
			leftWindow : winLateral,
			centerWindow : winCentral,
			showShadow : true,
			animationMode : NappDrawerModule.ANIMATION_SLIDE_SCALE,
			backgroundColor : 'white',
			closeDrawerGestureMode : isAndroid ? NappDrawerModule.MODE_MARGIN : NappDrawerModule.CLOSE_MODE_ALL,
			openDrawerGestureMode : NappDrawerModule.OPEN_MODE_ALL,
			leftDrawerWidth : 320 * formulaWidth,
			top : 0
		});
	} else {
		drawer = NappDrawerModule.createDrawer({
			backgroundColor : 'white',
			navBarHidden : true,
			leftWindow : winLateral,
			centerWindow : winCentral,
			fading : 0.2, // 0-1
			parallaxAmount : 0.2, //0-1
			showShadow : true,
						leftDrawerWidth : 320 * formulaWidth,

			rightDrawerWidth : 320 * formulaWidth,
			animationMode : NappDrawerModule.ANIMATION_NONE,
			closeDrawerGestureMode : NappDrawerModule.MODE_MARGIN,
			openDrawerGestureMode : NappDrawerModule.MODE_NONE,
			orientationModes : [Ti.UI.PORTRAIT]
		});
	}

	drawer.open();

	drawer.addEventListener('android:back', function() {
		var alertDialog = Titanium.UI.createAlertDialog({
			title : abandonar_app,
			buttonNames : [si, no]
		});
		alertDialog.show();

		alertDialog.addEventListener('click', function(e) {
			
			if (e.index == 0) {
				if (Ti.App.audioPlayer != null) {
					Ti.App.audioPlayer.pause();
				}
				Ti.App.fireEvent("tancaVideo", {});
				var activity = Titanium.Android.currentActivity;
				activity.finish();
			} else if (e.index == 1) {
				alertDialog.hide();
			}
		});

	});

	downloaderWindowInstance.close();
}

