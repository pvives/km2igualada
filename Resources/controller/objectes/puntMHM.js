var idioma;
if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
	idioma = 'ca';
} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
	idioma = 'es';
} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
	idioma = 'en';
} else if (Titanium.App.Properties.getString("language", "undefined") == 'ar') {
	idioma = 'ar';
} else {
	idioma = 'en';
}
if (Titanium.Platform.osname === 'android') {
	var isAndroid = true;
}
// var imageFactory = require('ti.imagefactory');

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
var heightpantalla = Ti.Platform.displayCaps.platformHeight;
var formulaWidth = widthpantalla / 320;
var formulaHeight = heightpantalla / 480;
var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;
if (Ti.Platform.osname == 'android') {
	var customFont = 'gotham-rounded-medium';
	var customFontBarraSuperior = 'gotham-rounded-medium';
	var customFontBold = 'gotham-rounded-medium';
} else {
	var customFont = 'Gotham Rounded';
	var customFontBarraSuperior = 'Gotham Rounded';
	var customFontBold = 'Gotham Rounded';
}
var nomLoguejat = Titanium.App.Properties.getString("user");

function createFitxaPuntMHM(win, idEscenari, data, mapView) {

	var labelTitolPuntMHM;

	var postUrlAportacio = urlBase + '/api/PuntMHM/Aportacio/crea/mhm';
	var postURLEdicioAportacio = urlBase + '/api/PuntMHM/Aportacio/update/mhm';

	var scrollableViewPuntMHM = null;

	var fitxaPuntMHM = Ti.UI.createView({
		width : 320 * formulaWidth,
		zIndex : 20,
		top : iOS7 ? 20 : 0 * formulaHeight,
		left : 0,
		height : isAndroid ? 480 * formulaHeight : iOS7 ? 480 * formulaHeight - 20 : 480 * formulaHeight,
		backgroundColor : 'white'
	});

	//creo view PuntMHM
	var barraSuperiorPunt = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44,
		backgroundColor : Ti.App.colorPrincipal,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth
	});
	fitxaPuntMHM.add(barraSuperiorPunt);
	// if (Ti.App.idEscenari != Ti.App.idEscenariMHM) {
	// var afegirAportacioButton = Ti.UI.createView({
	// width : 44 * formulaWidth,
	// height : isAndroid ? 44 * formulaHeight : 44,
	// right : 0 * formulaWidth + 5 * formulaWidth,
	// top : 0 * formulaHeight,
	// bubbleParent : true,
	// zIndex : 22,
	// backgroundImage : '/images/create.png'
	// });
	// barraSuperiorPunt.add(afegirAportacioButton);
	//
	// if (Ti.App.isTablet) {
	// afegirAportacioButton.backgroundImage = '/imagesTablet/create.png';
	// }
	// afegirAportacioButton.addEventListener('click', function(e) {
	//
	// if (loggedIn == 'true') {
	// var fitxaAportacioMHM = require('controller/objectes/creacioPunt');
	// var fitxaAportacioInstance = new fitxaAportacioMHM(win, 'creacioAportacio', win, mapView, Ti.App.idEscenari, data, fitxaPuntMHM);
	// fitxaPuntMHM.add(fitxaAportacioInstance);
	// } else {
	// alert(loguejat_punts);
	// }
	// });
	// }
	var botoBack = Ti.UI.createView({
		width : 44 * formulaHeight,
		height : isAndroid ? 44 * formulaHeight : 44,
		left : 5 * formulaWidth,
		top : 0 * formulaHeight,
		bubbleParent : true,
		backgroundImage : '/images/close.png'
	});
	barraSuperiorPunt.add(botoBack);

	if (Ti.App.isTablet) {
		botoBack.backgroundImage = '/imagesTablet/close.png';
	}

	botoBack.addEventListener("click", function() {
		if (Ti.App.audioPlayer != null) {
			Ti.App.audioPlayer.pause();
		}
		Ti.App.fireEvent("tancaVideo", {});
		win.backgroundColor = Ti.App.colorPrincipal;
		// Ti.App.webView.url='';
		// Ti.App.webView.repaint();
		if (fitxaPuntMHM != null) {
			win.remove(fitxaPuntMHM);
		}

		fitxaPuntMHM = null;
	});

	labelAportacioActual = Ti.UI.createLabel({
		text : '',
		left : 34 * formulaWidth,
		textAlign : 'center',
		// backgroundColor:'red',
		height : isAndroid ? 25 * formulaHeight : 22,
		width : 320 * formulaWidth - 34 * formulaWidth - 44 * formulaWidth,
		color : 'white',
		backgroundColor : 'transparent',
		bottom : 0 * formulaHeight,
		font : {
			fontSize : 11 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFontBarraSuperior
		}
	});

	labelTitolPuntMHM = Ti.UI.createTextField({
		editable : false,
		top : isAndroid ? 1 * formulaHeight : 0,
		left : 34 * formulaWidth,
		color : 'white',
		clearOnEdit : false,
		zIndex : 10,
		hintText : escriu_punt,
		backgroundFocusedColor : 'transparent',
		returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
		keyboardType : Titanium.UI.KEYBOARD_ASCII,
		enableReturnKey : true,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height : isAndroid ? 38 * formulaHeight : 38,
		textAlign : 'center',
		width : 320 * formulaWidth - 34 * formulaWidth - 44 * formulaWidth,
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFontBarraSuperior
		}
	});
	barraSuperiorPunt.add(labelTitolPuntMHM);

	var loggedIn = Titanium.App.Properties.getString("loggedIn");

	var arrayaportacions = [];
	if (scrollableViewPuntMHM != null) {
		fitxaPuntMHM.remove(scrollableViewPuntMHM);
	}

	barraSuperiorPunt.add(labelAportacioActual);

	// if (Ti.App.idEscenari != Ti.App.idEscenariMHM) {
	// var botoAfegirAportacio = Ti.UI.createView({
	// width : 44 * formulaWidth,
	// height : isAndroid ? 44 * formulaHeight : 44,
	// right : 0 * formulaWidth,
	// top : 0 * formulaHeight,
	// bubbleParent : true,
	// backgroundImage : '/images/create.png'
	// });
	// barraSuperiorPunt.add(botoAfegirAportacio);
	//
	// botoAfegirAportacio.addEventListener("click", function() {
	// if (loggedIn == 'true') {
	// var arrayTotalAportacions = [];
	// arrayTotalAportacions.push(scrollableViewPuntMHM.views);
	//
	// var fitxaAportacioMHM = require('controller/objectes/creacioAportacio');
	// var fitxaAportacioInstance = new fitxaAportacioMHM();
	//
	// arrayTotalAportacions.push(fitxaAportacioInstance);
	//
	// scrollableViewPuntMHM.views = arrayTotalAportacions;
	// scrollableViewPuntMHM.scrollToView(scrollableViewPuntMHM.views.length - 1);
	// } else {
	// alert(loguejat_crear_aportacions);
	// }
	// });
	// }

	if (data != null) {
		if (idioma == 'ca') {
			if (data.nomcaES != null && data.nomcaES != '') {
				labelTitolPuntMHM.value = data.nomcaES;
			} else {
				labelTitolPuntMHM.value = data.nom;
			}

		} else if (idioma == 'es') {
			if (data.nomes != null && data.nomes != '') {
				labelTitolPuntMHM.value = data.nomes;
			} else {
				labelTitolPuntMHM.value = data.nom;
			}

		} else if (idioma == 'en') {
			if (data.nomen != null && data.nomen != '') {
				labelTitolPuntMHM.value = data.nomen;
			} else {
				labelTitolPuntMHM.value = data.nom;
			}

		} else if (idioma == 'ar') {

			if (data.nomar != null && data.nomar != '') {
				labelTitolPuntMHM.value = data.nomar;
			} else {
				labelTitolPuntMHM.value = data.nom;
			}

		} else {
			labelTitolPuntMHM.value = data.nom;
		}

	}

	scrollableViewPuntMHM = Ti.UI.createScrollableView({
		top : isAndroid ? 44 * formulaHeight : 44,
		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 : 480 * formulaHeight - 44 - 20,
		left : 0 * formulaWidth,
		width : 320 * formulaWidth,
		views : [],
		scrollingEnabled : false,
		visible : true,
		showPagingControl : isAndroid ? false : false,
		maxZoomScale : 2.0,

	});

	fitxaPuntMHM.add(scrollableViewPuntMHM);

	if (data != null) {
		if (data.aportacions != null && data.aportacions != '') {

			for (var i = 0; i < data.aportacions.length; i++) {

				var fitxaAportacioMHM = require('controller/objectes/Mod_objecteAportacio');
				var fitxaAportacioInstance = new fitxaAportacioMHM(data, i, scrollableViewPuntMHM, labelAportacioActual);
				arrayaportacions[i] = fitxaAportacioInstance;
				if (i == data.aportacions.length - 1) {
					scrollableViewPuntMHM.views = arrayaportacions;

					labelAportacioActual.text = (scrollableViewPuntMHM.currentPage + 1) + ' de ' + (scrollableViewPuntMHM.views.length);
				}
			}
		} else {
			var fitxaAportacioInstance = Ti.UI.createView({
				width : 320 * formulaWidth,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 0,
				layout : 'vertical',
				height : isAndroid ? 480 * formulaHeight : 480 * formulaHeight
			});

			var labelDescripcio = Ti.UI.createLabel({
				clearOnEdit : false,
				zIndex : 15,
				text : data.descripcio,
				backgroundFocusedColor : 'transparent',
				autocorrect : false,
				hintText : '+ ' + descripcio,
				returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
				keyboardType : Titanium.UI.KEYBOARD_ASCII,
				enableReturnKey : true,
				verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
				editable : false,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 10 * formulaWidth,
				height : Ti.UI.SIZE,
				color : Ti.App.colorPrincipal,
				textAlign : 'left',
				width : 300 * formulaWidth,
				font : {
					fontSize : 12 + Ti.App.paddingLabels + 'sp',
					fontFamily : customFont
				}
			});

			fitxaAportacioInstance.add(labelDescripcio);
			if (data.capaprincipal != null && data.capaprincipal != '') {
				var labelCapa = Ti.UI.createLabel({
					clearOnEdit : false,
					zIndex : 15,
					text : data.capaprincipal.nom,
					backgroundFocusedColor : 'transparent',
					autocorrect : false,
					hintText : '+ ' + descripcio,
					returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
					keyboardType : Titanium.UI.KEYBOARD_ASCII,
					enableReturnKey : true,
					verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
					editable : false,
					top : isAndroid ? 0 * formulaHeight : 0,
					left : 10 * formulaWidth,
					height : Ti.UI.SIZE,
					color : Ti.App.colorPrincipal,
					textAlign : 'left',
					width : 300 * formulaWidth,
					font : {
						fontSize : 11 + Ti.App.paddingLabels + 'sp',
						fontFamily : customFont
					}
				});

				fitxaAportacioInstance.add(labelCapa);

				var labelCategoria = Ti.UI.createLabel({
					clearOnEdit : false,
					zIndex : 15,
					text : data.categoriaprincipal.nom,
					backgroundFocusedColor : 'transparent',
					autocorrect : false,
					hintText : '+ ' + descripcio,
					returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
					keyboardType : Titanium.UI.KEYBOARD_ASCII,
					enableReturnKey : true,
					verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
					editable : false,
					top : isAndroid ? 0 * formulaHeight : 0,
					left : 10 * formulaWidth,
					height : Ti.UI.SIZE,
					color : Ti.App.colorPrincipal,
					textAlign : 'left',
					width : 300 * formulaWidth,
					font : {
						fontSize : 11 + Ti.App.paddingLabels + 'sp',
						fontFamily : customFont
					}
				});

				fitxaAportacioInstance.add(labelCategoria);
			}

			arrayaportacions[0] = fitxaAportacioInstance;
			scrollableViewPuntMHM.views = arrayaportacions;
		}

	}

	return fitxaPuntMHM;

}

module.exports = createFitxaPuntMHM;
