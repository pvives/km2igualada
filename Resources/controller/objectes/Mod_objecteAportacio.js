function editarAportacio(dataAportacio, i, scrollableViewPuntMHM, labelAportacioActual) {
	var win = Ti.UI.currentWindow;
	var nomLoguejat = Titanium.App.Properties.getString("user");
	var idioma;
	var labelNomUsuari;
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		idioma = 'ca';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		idioma = 'es';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		idioma = 'en';
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'ar') {
		idioma = 'ar';
	} else {
		idioma = 'en';
	}

	var fitxaAportacio = Ti.UI.createView({
		width : 320 * formulaWidth,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0,
		height : isAndroid ? 480 * formulaHeight : 480 * formulaHeight
	});

	var aportacioViewFitxa = Ti.UI.createScrollView({
		width : 320 * formulaWidth,
		layout : 'vertical',
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,

		height : isAndroid ? 480 * formulaHeight - statusBarHeight - 44 * formulaHeight : 480 * formulaHeight - 20 - 44,
		contentHeight : Ti.UI.SIZE
	});

	fitxaAportacio.add(aportacioViewFitxa);

	var aportacioView = Ti.UI.createView({
		width : 320 * formulaWidth,
		layout : 'vertical',
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,

		height : Ti.UI.SIZE
	});

	aportacioViewFitxa.add(aportacioView);

	var liniaNegra = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 1 * formulaHeight : 1,
		left : 0 * formulaWidth,
		zIndex : 10,
		top : isAndroid ? 0 : 0,
		backgroundColor : 'black'

	});
	fitxaAportacio.add(liniaNegra);

	var dataAportacioView = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : Ti.UI.SIZE,
		top : isAndroid ? 10 * formulaHeight : 10,
		backgroundColor : 'transparent',
		left : 0 * formulaWidth
	});
	aportacioView.add(dataAportacioView);

	var dataAportacioViewCentral = Ti.UI.createView({
		width : 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
		height : Ti.UI.SIZE,
		top : isAndroid ? 0 * formulaHeight : 0,
		// backgroundColor : '#f2f2f2',
		layout : 'vertical',
		left : 44 * formulaWidth
	});
	dataAportacioView.add(dataAportacioViewCentral);

	// var viewLikes = Ti.UI.createView({
	// width : 25 * formulaWidth,
	// height : isAndroid ? 25 * formulaHeight : 25,
	// left : 37 * formulaWidth,
	//
	// top : isAndroid ? 12 * formulaHeight : 12,
	// backgroundImage : '/images/heart.png'
	//
	// });
	// dataAportacioView.add(viewLikes);

	// if (Ti.App.isTablet) {
	// viewLikes.backgroundImage = '/imagesTablet/heart.png';
	// }
	//
	// var labelNumLikes = Ti.UI.createLabel({
	// top : isAndroid ? 35 * formulaHeight : 35,
	// left : 30 * formulaWidth,
	// color : 'black',
	// height : Ti.UI.SIZE,
	// textAlign : 'center',
	// width : 39 * formulaWidth,
	// font : {
	// fontSize : 12 + Ti.App.paddingLabels + 'sp',
	// fontFamily : customFontBold
	// }
	// });
	// dataAportacioView.add(labelNumLikes);
	// if (dataAportacio != null) {
	// labelNumLikes.text = dataAportacio.aportacions[i].likes;
	// }
	//
	// var barraLike = require('controller/objectes/barraLike');
	// var barraLikeInstance = new barraLike(dataAportacio, i, labelNumLikes);
	// fitxaAportacio.add(barraLikeInstance);

	var fletxaEsquerraPuntMHM = Ti.UI.createView({
		width : 44 * formulaHeight,
		height : isAndroid ? 44 * formulaHeight : 44 * formulaHeight,
		left : 0 * formulaWidth,
		zIndex : 10,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundImage : '/images/previous.png'

	});
	if (dataAportacio.aportacions.length > 1) {
		dataAportacioView.add(fletxaEsquerraPuntMHM);

	}

	if (Ti.App.isTablet) {
		fletxaEsquerraPuntMHM.backgroundImage = '/imagesTablet/previous.png';
	}

	fletxaEsquerraPuntMHM.addEventListener('click', function(e) {
		if (scrollableViewPuntMHM.currentPage > 0) {
			labelAportacioActual.text = (scrollableViewPuntMHM.currentPage) + ' de ' + (scrollableViewPuntMHM.views.length);
			scrollableViewPuntMHM.scrollToView(scrollableViewPuntMHM.currentPage - 1);
		}
	});

	var fletxaDretaPuntMHM = Ti.UI.createView({
		width : 44 * formulaHeight,
		height : isAndroid ? 44 * formulaHeight : 44 * formulaHeight,
		right : 0 * formulaWidth,
		zIndex : 10,
		top : isAndroid ? 0 * formulaHeight : 0,
		backgroundImage : '/images/next.png'

	});
	if (dataAportacio.aportacions.length > 1) {

		dataAportacioView.add(fletxaDretaPuntMHM);
	}
	if (Ti.App.isTablet) {
		fletxaDretaPuntMHM.backgroundImage = '/imagesTablet/next.png';
	}

	fletxaDretaPuntMHM.addEventListener('click', function(e) {
		if (scrollableViewPuntMHM.currentPage < scrollableViewPuntMHM.views.length - 1) {
			labelAportacioActual.text = (scrollableViewPuntMHM.currentPage + 2) + ' de ' + (scrollableViewPuntMHM.views.length);
			scrollableViewPuntMHM.scrollToView(scrollableViewPuntMHM.currentPage + 1);
		}
	});

	var labelNomAportacio = Ti.UI.createLabel({
		// editable : false,
		// hintText : escriu_aportacio,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,
		color : Ti.App.colorPrincipal,
		// clearOnEdit : false,
		// leftButtonPadding : 9,
		zIndex : 15,
		// autoCorrect : false,
		backgroundFocusedColor : 'transparent',
		// returnKeyType : Titanium.UI.RETURNKEY_NEXT,
		// keyboardType : Titanium.UI.KEYBOARD_ASCII,
		// verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height : Ti.UI.SIZE,
		textAlign : 'center',
		width : 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFontBarraSuperior
		}
	});
	dataAportacioViewCentral.add(labelNomAportacio);

	if (dataAportacio != null) {
		if (idioma == 'ca') {
			if (dataAportacio.aportacions[i].nomcaES != null && dataAportacio.aportacions[i].nomcaES != '') {
				labelNomAportacio.text = dataAportacio.aportacions[i].nomcaES;
			} else {
				labelNomAportacio.text = dataAportacio.aportacions[i].nom;
			}
		} else if (idioma == 'es') {
			if (dataAportacio.aportacions[i].nomes != null && dataAportacio.aportacions[i].nomes != '') {
				labelNomAportacio.text = dataAportacio.aportacions[i].nomes;
			} else {
				labelNomAportacio.text = dataAportacio.aportacions[i].nom;
			}
		} else if (idioma == 'en') {
			if (dataAportacio.aportacions[i].nomen != null && dataAportacio.aportacions[i].nomen != '') {
				labelNomAportacio.text = dataAportacio.aportacions[i].nomen;
			} else {
				labelNomAportacio.text = dataAportacio.aportacions[i].nom;
			}
		} else if (idioma == 'ar') {
			if (dataAportacio.aportacions[i].nomar != null && dataAportacio.aportacions[i].nomar != '') {
				labelNomAportacio.text = dataAportacio.aportacions[i].nomar;
			} else {
				labelNomAportacio.text = dataAportacio.aportacions[i].nom;
			}
		} else {
			labelNomAportacio.text = dataAportacio.aportacions[i].nom;
		}
	}

	//capa categoria

	var labelCapaCategoria = Ti.UI.createLabel({
		top : isAndroid ? 0 * formulaHeight : 0,
		text : capa_categoria,
		left : 0 * formulaWidth,
		color : 'black',
		touchEnabled : true,
		textAlign : 'center',
		height : Ti.UI.SIZE,
		textAlign : 'center',
		width : 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFont
		}
	});
	dataAportacioViewCentral.add(labelCapaCategoria);

	if (dataAportacio != null) {
		labelCapaCategoria.text = dataAportacio.aportacions[i].capa.nom + ' - ' + dataAportacio.aportacions[i].categoria.nom;
		labelCapaCategoria.idCapa = dataAportacio.aportacions[i].capa.id;
		labelCapaCategoria.idCategoria = dataAportacio.aportacions[i].categoria.id;
	}

	//autor
	labelNomUsuari = Ti.UI.createLabel({
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 0 * formulaWidth,
		color : '#9c9e9f',
		height : Ti.UI.SIZE,
		textAlign : 'center',
		width : 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth,
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFont
		},
		labelNomAportacio : labelNomAportacio
	});

	if (dataAportacio.aportacions[i].creador != null) {
		labelNomUsuari.text = dataAportacio.aportacions[i].creador;
	}

	dataAportacioViewCentral.add(labelNomUsuari);

	//galeria imatges
	if (dataAportacio.aportacions[i].imatges != null && dataAportacio.aportacions[i].imatges != '') {

		// alert(dataAportacio.aportacions[i].imatges[j].);
		if (dataAportacio.aportacions[i].imatges.length != 0) {

			var scrollableGalleryView = null;
			scrollableGalleryView = Ti.UI.createScrollableView({
				top : isAndroid ? 10 * formulaHeight : 10,
				height : isAndroid ? 200 * formulaWidth : 200,
				width : 300 * formulaWidth,
				views : [],
				scrollingEnabled : true,
				left : isAndroid ? 10 * formulaWidth : 10 * formulaWidth,
				backgroundColor : 'transparent',
				maxZoomScale : 2.0
			});

			galleryImageViews = [];

			for (var j = 0; j < dataAportacio.aportacions[i].imatges.length; j++) {

				var view = Ti.UI.createImageView({
					backgroundColor : 'transparent',
					j : j,
					hihaFotos : true,
					width : Ti.UI.SIZE,
					height : Ti.UI.SIZE,
					viewAfegirFoto : false,
					image : Ti.App.urlBase + dataAportacio.aportacions[i].imatges[j].thumb600x400
				});
				galleryImageViews[j] = view;
			}
			scrollableGalleryView.views = galleryImageViews;

			var pagingControl = require('controller/objectes/pagingControl');
			var pagingControlInstance = new pagingControl(scrollableGalleryView);

			aportacioView.add(scrollableGalleryView);
			aportacioView.add(pagingControlInstance);
		}

	}

	//Creo fitxa audio
	if (dataAportacio.aportacions[i].audio != null && dataAportacio.aportacions[i].audio != '') {
		var fitxaAudio = require('/controller/fitxesObjectes/fitxaAudio');
		fitxaAudioInstance = new fitxaAudio(dataAportacio, urlBase, i);
		aportacioView.add(fitxaAudioInstance);
	}

	//Creo fitxa web
	if (dataAportacio.aportacions[i].url != null && dataAportacio.aportacions[i].url != '') {
		// var viewURL = Ti.UI.createView({
		// width : Ti.UI.SIZE,
		// backgroundColor : Ti.App.colorSecundari,
		// linkActivat : true,
		// url : dataAportacio != null ? dataAportacio.aportacions[i].url : null,
		// left : isAndroid ? 10 * formulaWidth : 10,
		// height : isAndroid ? 28 * formulaHeight : 28,
		// top : isAndroid ? 10 * formulaHeight : 10
		//
		// });
		// aportacioView.add(viewURL);

		// var logoURL = Ti.UI.createView({
		// width : 36 * formulaWidth,
		// left : isAndroid ? 0 * formulaWidth : 0,
		// height : isAndroid ? 30 * formulaHeight : 30,
		// top : isAndroid ? 0 * formulaHeight : 0,
		// backgroundImage : '/images/urlimage.png'
		// });
		// viewURL.add(logoURL);
		// viewURL.logoURL = logoURL;

		// if (Ti.App.isTablet) {
		// logoURL.backgroundImage = '/imagesTablet/urlimage.png';
		// }

		var labelWeb = Ti.UI.createLabel({
			zIndex : 15,
			text : dataAportacio.aportacions[i].url,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			backgroundColor : 'transparent',
			color : '#0000ff',
			textAlign : 'left',
			width : 290 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		labelWeb.addEventListener('click', function(k) {

			Titanium.Platform.openURL(dataAportacio.aportacions[i].url);

		});

		aportacioView.add(labelWeb);
	}

	var descripcio = require('controller/fitxesObjectes/descripcioAportacio');
	var descripcioInstance = new descripcio(dataAportacio, i, aportacioView);

	//Creo fitxa video
	if (dataAportacio.aportacions[i].video != '' && dataAportacio.aportacions[i].video != null) {
		var fitxaVideo = require('/controller/fitxesObjectes/fitxaVideo');
		var fitxaVideoInstance = new fitxaVideo(dataAportacio, urlBase, i);
		aportacioView.add(fitxaVideoInstance);
	}

	if (dataAportacio.aportacions[i].segle != null) {
		var labelSegleTitol = Ti.UI.createLabel({
			text : segle,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelSegleTitol);

		var labelSegle = Ti.UI.createLabel({
			clearOnEdit : false,
			zIndex : 15,
			text : dataAportacio.aportacions[i].segle,

			backgroundFocusedColor : 'transparent',
			autocorrect : false,
			hintText : '+ ' + descripcio,
			returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
			keyboardType : Titanium.UI.KEYBOARD_ASCII,
			enableReturnKey : true,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
			editable : false,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : 'black',
			textAlign : 'left',
			width : 300 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelSegle);
	}

	if (dataAportacio.aportacions[i].any != null) {
		var labelAnyTitol = Ti.UI.createLabel({
			text : any,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelAnyTitol);

		var labelAny = Ti.UI.createLabel({
			clearOnEdit : false,
			zIndex : 15,
			text : dataAportacio.aportacions[i].any,

			backgroundFocusedColor : 'transparent',
			autocorrect : false,
			hintText : '+ ' + descripcio,
			returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
			keyboardType : Titanium.UI.KEYBOARD_ASCII,
			enableReturnKey : true,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
			editable : false,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : 'black',
			textAlign : 'left',
			width : 300 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelAny);
	}

	if (dataAportacio.aportacions[i].mes != null) {
		var labelMesTitol = Ti.UI.createLabel({
			text : mes,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelMesTitol);

		var labelMes = Ti.UI.createLabel({
			clearOnEdit : false,
			zIndex : 15,
			text : dataAportacio.aportacions[i].mes,

			backgroundFocusedColor : 'transparent',
			autocorrect : false,
			hintText : '+ ' + descripcio,
			returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
			keyboardType : Titanium.UI.KEYBOARD_ASCII,
			enableReturnKey : true,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
			editable : false,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : 'black',
			textAlign : 'left',
			width : 300 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelMes);
	}

	if (dataAportacio.aportacions[i].dia != null) {
		var labelDiaTitol = Ti.UI.createLabel({
			text : dia,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelDiaTitol);

		var labelDia = Ti.UI.createLabel({
			clearOnEdit : false,
			zIndex : 15,
			text : dataAportacio.aportacions[i].dia,

			backgroundFocusedColor : 'transparent',
			autocorrect : false,

			enableReturnKey : true,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
			editable : false,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : 'black',
			textAlign : 'left',
			width : 300 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelDia);
	}

	if (dataAportacio.aportacions[i].font != null) {
		var labelFontTitol = Ti.UI.createLabel({
			text : font,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelFontTitol);

		var labelFont = Ti.UI.createLabel({
			clearOnEdit : false,
			zIndex : 15,
			text : dataAportacio.aportacions[i].font,

			backgroundFocusedColor : 'transparent',
			autocorrect : false,
			hintText : '+ ' + descripcio,
			returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
			keyboardType : Titanium.UI.KEYBOARD_ASCII,
			enableReturnKey : true,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
			editable : false,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : 'black',
			textAlign : 'left',
			width : 300 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelFont);
	}

	if (dataAportacio.aportacions[i].vikipedia != null) {
		var labelVikipediaTitol = Ti.UI.createLabel({
			text : viquipedia,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelVikipediaTitol);

		var labelVikipedia = Ti.UI.createLabel({
			clearOnEdit : false,
			zIndex : 15,
			text : dataAportacio.aportacions[i].vikipedia,

			backgroundFocusedColor : 'transparent',
			autocorrect : false,
			hintText : '+ ' + descripcio,
			returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
			keyboardType : Titanium.UI.KEYBOARD_ASCII,
			enableReturnKey : true,
			verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
			editable : false,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : 'black',
			textAlign : 'left',
			width : 300 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelVikipedia);
		labelVikipedia.addEventListener('click', function(e) {
			Titanium.Platform.openURL(dataAportacio.aportacions[i].vikipedia);
		});
	}

	if (dataAportacio.aportacions[i].etiquetes != null) {

		var labelEtiquetesTitol = Ti.UI.createLabel({
			text : etiquetes,
			top : isAndroid ? 10 * formulaHeight : 10,
			left : 10 * formulaWidth,
			height : Ti.UI.SIZE,
			color : '#9c9e9f',
			textAlign : 'left',
			width : 280 * formulaWidth,
			font : {
				fontSize : 12 + Ti.App.paddingLabels + 'sp',
				fontFamily : customFont
			}
		});
		aportacioView.add(labelEtiquetesTitol);

		for (var j = 0; j < dataAportacio.aportacions[i].etiquetes.length; j++) {
			var labelEtiquetes = Ti.UI.createLabel({
				clearOnEdit : false,
				zIndex : 15,

				backgroundFocusedColor : 'transparent',
				autocorrect : false,
				text : dataAportacio.aportacions[i].etiquetes[j].etiqueta + ' ',
				hintText : '+ ' + descripcio,
				returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
				keyboardType : Titanium.UI.KEYBOARD_ASCII,
				enableReturnKey : true,
				verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
				editable : false,
				top : isAndroid ? 0 * formulaHeight : 0,
				left : 10 * formulaWidth,
				height : Ti.UI.SIZE,
				color : 'black',
				textAlign : 'left',
				width : 300 * formulaWidth,
				font : {
					fontSize : 12 + Ti.App.paddingLabels + 'sp',
					fontFamily : customFont
				}
			});
		}

		aportacioView.add(labelEtiquetes);
	}
	if (dataAportacio.aportacions[i].urlform != null) {

		var webView = Ti.UI.createWebView({
			url : dataAportacio.aportacions[i].urlform,
			enableZoomControls : true,
			scalesPageToFit : true,
			zIndex : 10,
			top : 5 * formulaHeight,
			width : 320 * formulaWidth,
			height : Ti.UI.SIZE,
			scrollsToTop : false,
			showScrollbars : false
		});

		aportacioView.add(webView);
	}

	var padding = Ti.UI.createView({
		width : 320 * formulaWidth,
		height : isAndroid ? 50 * formulaHeight : 50,
		left : 0 * formulaWidth,
		backgroundColor : 'transparent',
		backgroundImage : 'none',
		top : isAndroid ? 0 : 0
	});
	aportacioView.add(padding);

	return fitxaAportacio;

}

module.exports = editarAportacio;
