var pageColor =Ti.App.colorSecundari;

PagingControl = function(scrollableView){

var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;
		
	 	// Keep a global reference of the available pages
	var numberOfPages = scrollableView.getViews().length;
	
	var container = Titanium.UI.createView({
		height: Ti.UI.SIZE,
		visible:numberOfPages>0?true:false,
		top:10*formulaHeight
	});

	
	var pages = []; // without this, the current page won't work on future references of the module
	
	// Go through each of the current pages available in the scrollableView
	for (var i = 0; i < numberOfPages; i++) {
		var page = Titanium.UI.createView({
			borderRadius: 4,
			width: 8*formulaWidth,
			height: 8*formulaWidth,
			left: (15*formulaWidth) * i,
			backgroundColor: pageColor,
			opacity: 0.5
		});
		// Store a reference to this view
		pages.push(page);
		// Add it to the container  
		container.add(page);
	}
	
	// Mark the initial selected page
	if(numberOfPages>0){
		pages[scrollableView.getCurrentPage()].setOpacity(1);
		container.left=(320*formulaWidth-(15*formulaWidth*numberOfPages))/2;
	}
	
	
	
	// Callbacks
	onScroll = function(event){
		if(scrollableView.getCurrentPage()>=0&&scrollableView.getCurrentPage()<numberOfPages){
					// Go through each and reset it's opacity
		for (var i = 0; i < numberOfPages; i++) {
			if(event.currentPage==i){
				pages[event.currentPage].setOpacity(1);
			}
			else{
				pages[i].setOpacity(0.5);
			}
			
		}
		// Bump the opacity of the new current page
		
		}

		
	};
	
	// Attach the scroll event to this scrollableView, so we know when to update things
	scrollableView.addEventListener("scroll", onScroll);
	
	return container;
};
 
module.exports = PagingControl;