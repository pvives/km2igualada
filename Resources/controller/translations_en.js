var escenaris='Scenarios';
var escenaris2 = ' scenarios';

var titol='Title';
var autor='Author';
var ambit_geografic= 'Geographic scope';
var labelIdiomaText='LANGUAGE';
var labelIdiomaTextMinuscula='Language';
var area_coneixement='Subject Area';
var public_objectiu='Target audience';
var edat_autor='Author\'s age';
var centre_educatiu='School institution';
var populars='Popular';
var recents='Recent';
var destacats='Featured';
var els_meus='My scenarios';
var buscar='Search';
var inici_joc='Start game';
var cerca_escenari='Search scenario';
var _8_10_anys='8-10 years';
var _10_12_anys='10-12 years';
var _12_14_anys='12-14 years';
var _14_17_anys='14-17 years';
var _17_25_anys='17-25 years';
var _25_60_anys='25-60 years';
var majors_60='Over 60 years old';
var done='Done';
var ciencies_socials='Social Sciences';
var llengua_literatura='Language and Literature';
var llengues_estrangeres='Foreign Languages';
var matematiques='Mathematics';
var musica='Music';
var ciencies_naturals='Natural Sciences';
var educacio_fisica='Physical Education';
var educacio_artistica='Visual arts';
var oci_temps_lliure='Leisure and free time';
var altres='Others';
var inici_trivial= "Start";
var filtre_avancat='Show advanced filter';
var iniciar='Start';
var alerta_current_location_title="Careful!/Attention!";
var alerta_current_location_message="Couldn't get your current location. Check if you have location service turned off";
var calculant_distancia='Calculating distance...';	
var xhr_error_title="Attention";
var xhr_error_message="Can not connect with the server. Check your internet connection.";
var no_has_arribat='You need to solve this clue to pass to the next one';
var carregant="Loading...";
var pull_down_refresh="Pull down to refresh...";
var pull_to_reload="Pull to reload";
var release_to_refresh="Release to refresh...";
var ultima_actualitzacio= "Last Updated: ";
var nomUsuari='Username';
var password='Password';
var entrar='Enter		';
var crea_nou_punt="Create a new point";
var afegir_foto="Add a picture";
var afegir_text="Add text";
var capa="Layer";
var escull_capa="Choose a layer";
var recurs_servei="Resource/Service";
var escull_recurs_servei="Choose a resource/service";
var publicar ="Publish";
var descarregant='Downloading...';
var biblioteca='Library';
var meus_escenaris='My scenarios';
var descarregats='Downloads';
var buscador='Search engine';
var login='LOGIN';
var  buscar_titulo='Sort by Title';
var obrir='OPEN';
var escenari_descarregat='This scenario was already downloaded';
var cal_estar_loguejat='Log on to view your scenarios.';
var paraula_clau='Title / key word';
var filtre='Filter';
var filtrar='Applying filter...';
var activar_filtre='Enable filter';
var desactivar_filtre='Disable filter';
var contrasenya_incorrecta='Wrong password, try it again.';
var usuari_no_existeix='This username does not exist. Try it again.';
var logged_in_as='You are logged in as:';

var tots= 'All';
var totes='All';
var esborrat_correctament='Scenario deleted successfully';
var  ultima_actualitzacio_escenari='Last update';
var info_descarregats = '- You can download scenario contents (objects, photos, audio) from the detail card. \n\n- An Internet connection is required to access videos and links. \n\n- Map is not downloadable. When online, deveice will save the last pictures from the map to show them offline later.\n\nTip:\nWhen online, move the map around the area and zoom in/out the parts of the map you will work on. This way, the map is more accessible when you are not connected.\n\n- To delete a downloaded scenario, clic on the Trash can icon on the list of downloaded items.\n\n';

var filtre_actiu='Filter on: \n';
var filtre_inactiu='Filtre off: \n';
var escenaris_filtre=' resulting scenarios';
var restants = ' meet the criteria';
var escenari_sense_objectes = 'No object in this scenario.';
var vols_descarregar = 'Are you sure you want to download the contents of this Scenario?';
var si= 'Yes';
var no='No';
var no_escenaris_descarregats='No scenario downloaded.';
var no_escenaris_descarregats_message='Download scenario from detail card.';


var descarregat_correctament = 'Scenario downloaded successfully';
var consultar_descarregats = 'You can view the downloaded scenarios from the Downloads menu.'; 
var canvi_idioma='Restart the application to make language change effective.';
var configuracio='SETTINGS';
var pista_arribar='You must discover the place related to this clue to access the next clue.';
var actualitzar_app='A new version of this application is available. Please update to ensure it operates properly.';
var post_no_publicat_facebook='Please set up a Facebook account first in order to post your messages. Go to your device settings in the Facebook section.';
var post_no_publicat_twitter='Please set up a Twitter account first in order to post your tweets. Go to your device settings in the Twitter section';
var registre='Register to get an Eduloc account';
var tots='All';
var totes='All';
var altres_projectes='OTHER PROJECTS';
var no_objectes='No hi ha objectes!';
var escriu_aportacio='+ Contribution title';
var escriu_punt='+ Point title';
var capa_categoria=' + Layer / Category';
var descripcio='Description';
var editar='EDIT';
var afegir_foto='+ Add Foto';
var selecciona_capa='Select layer';
var capa='Layer';
var selecciona_categoria='Select Category';
var url_youtube='Cannot play video. The video must be a Youtube URL.';
var agrada='Like';
var loguejat_crear_aportacions='You have to be logged in to create contributions';
var nom_punt='+ Point title';
var loguejat_punts='Log in to create points';
var omplir_titol_punt='Enter point\'s title';
var omplir_titol_aportacio='Enter contribution\'s title';
var selecciona_capa='Select a layer';
var selecciona_categoria='Select a category';
var punts=' points';
var rutes=' routes';
var crear_punt='Create Point';
var idioma_reinici='To change de language the app will be restarted. ¿Do you want to proceed?';
var abandonar_app='¿Do you want to exit the application?';
var segle="Century:";
var any='Year';
var font='Font: ';
var viquipedia='Viquipedia: ';
var perfil='Creator\'s profile: ';
var etiquetes='Tags';
var mes='Month:';
var dia='Day';
var veure_video='Play';
var reproduir_audio='Reproduïr àudio';
var parar_audio='Parar audio';