function createNetworkController(callbackUrl) {
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}
//consulto la URL Base del projecte
var xhr = Titanium.Network.createHTTPClient();

xhr.onload = function() {

	var data = this.responseText;
	callbackUrl(data);
};

xhr.open('POST', 'http://dev-mhm.itinerarium.cat/api/geturl');
xhr.send();
xhr.onerror = function(e) {
		var alertDialog = Titanium.UI.createAlertDialog({
			title : xhr_error_title,
			message : xhr_error_message,
			buttonNames : ['ok']
		});
		alertDialog.show();
};
}
module.exports = createNetworkController;