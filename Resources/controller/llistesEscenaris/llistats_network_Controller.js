function createNetworkController(url, callback, actInd) {
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	var primeraVegada = true;
	var xhr = Titanium.Network.createHTTPClient();

	var saltedPassword = Titanium.App.Properties.getString("saltedPassword", null);
	var userName = Titanium.App.Properties.getString("user");

	var wsse = require('controller/llistesEscenaris/wsse');
	var wsseHeader = wsse.wsseHeader(userName, saltedPassword);

	xhr.onload = function() {

		var arrayObjectes = [];
		var aux;
		var data = JSON.parse(this.responseText);
		
		if (Ti.App.ordenacio == 'valoracio_mitjana') {
			// ordeno eles escenaris segons la formula
			if(data.length!=1&&data.length!=0){
				
			for (var i = 0; i < data.length; i++) {
				arrayObjectes.push(data[i]);

				if (i == data.length - 1) {
				
					for (var k = 0; k < data.length; k++) {

						for (var j = 0; j < data.length; j++) {

							if (arrayObjectes[k].valoracions.formula > arrayObjectes[j].valoracions.formula) {

								aux = arrayObjectes[j];

								arrayObjectes[j] = arrayObjectes[k];

								arrayObjectes[k] = aux;
							}

						}
						if (k == data.length - 1) {
							
							callback(arrayObjectes);

						}

						//crida a createAnnotations
					}
				}
			}
}
else{
	callback(data);
}
		} else {
			callback(data);
			
		 }
		//crida a createAnnotations

	};

	xhr.open('POST', url);
	// xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	xhr.setRequestHeader('X-WSSE', wsseHeader);
	xhr.onerror = function(e) {

		//this fires if Titanium/the native SDK cannot successfully retrieve a resource
		if (primeraVegada == true) {
			if (actInd != null) {

				actInd.hide();
				fonsActInd.hide();

			}

			var alertDialog = Titanium.UI.createAlertDialog({
				title : xhr_error_title,
				message : xhr_error_message,
				buttonNames : ['Ok']
			});
			alertDialog.show();
			primeraVegada = false;
		}
	};

	xhr.send({
		'keywords' : Ti.App.paraula_clau,
		'filter_order_by' : Ti.App.ordenacio,
		'creador' : Ti.App.autor,
		'ambit' : Ti.App.poblacio,
		'idiomes' : Ti.App.idioma,
		'escola' : Ti.App.nom_centre,
		'competencies' : Ti.App.area_coneixement,
		'edatAutors' : Ti.App.edat_autor,
		'targets' : Ti.App.public_objectiu

	});

}

module.exports = createNetworkController;
