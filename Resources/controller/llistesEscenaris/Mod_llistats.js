function llistat(urlBase, dataEscenaris, barraSuperiorMapa) {

	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var customFont = 'Apex New';
	var customFontBarraSuperior = 'Apex New';
	var customFontBold = 'Apex New';
	var primeraVegada;
	var xhr = Titanium.Network.createHTTPClient();
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	Ti.App.clicat = false;

	var table = null;

	var viewLlistat = Ti.UI.createView({
		width : 320 * formulaWidth,
		backgroundColor : Ti.App.colorPrincipal,
		zIndex : 2,
		height : isAndroid ? 480 * formulaHeight - 44 * formulaHeight -statusBarHeight: iOS7 ? 480 * formulaHeight - 44 - 20 : 480 * formulaHeight - 44,
		top : iOS7 ? 20 + 44 : isAndroid ? 44 * formulaHeight : 44,
		left : 0 * formulaWidth,

	});

	creoLlista(dataEscenaris);

	function creoLlista(dataEscenaris) {

		var arrayRows = [];
		var conexio = false;
		var firsttime = true;
		var firstFocus = true;
		var clicat = false;
		var primerFocus = true;

		var paperera;
		var arrayObjectesEscenarisOffline = Titanium.App.Properties.getList("arrayObjectesEscenarisOffline", []);
		var idestacats = 0;
		var imatgeEscenari;
		var firstTime = true;
		if (dataEscenaris.length != 0) {

			for (var i = 0; i < dataEscenaris.length; i++) {
				if (i < 100) {
					if (Ti.App.idEscenariMHM != dataEscenaris[i].id) {
						var row = Ti.UI.createTableViewRow({
							height : Ti.UI.SIZE,
							width : 320 * formulaWidth,
							creador : dataEscenaris[i].creador,
							ambit : dataEscenaris[i].ambit,
							rightImage : '/images/flechalista.png',
							idioma : dataEscenaris[i].idioma,
							borderRadius : 0,
							backgroundSelectedColor : '#9c9e9f',
							objectesEscenariOffline : Ti.App.vincDescarregats ? arrayObjectesEscenarisOffline[i] : null,
							escenariJSON : dataEscenaris[i],
							bubbleParent : true,
							borderWidth : 10 * formulaHeight,
							borderColor : 'transparent',
							urlBase : urlBase,
							id : dataEscenaris[i].id,
							name : dataEscenaris[i].titulo,
							zIndex : 10,
							imatge : dataEscenaris[i].thumb560x280 != null ? dataEscenaris[i].thumb560x280 : '',
							selectedBackgroundColor : '#9c9e9f',
							backgroundColor : 'white',
							className : 'default'

						});

						var layout = Ti.UI.createView({
							height : Ti.UI.SIZE,
							width : 320 * formulaWidth,
							zIndex : 3,
							backgroundColor : 'transparent',
							layout : 'vertical',
							bubbleParent : true,
							top : 0 * formulaHeight,
							left : 0 * formulaWidth
						});
						row.add(layout);

						var viewImatge = Ti.UI.createImageView({
							width : 75 * formulaWidth,
							height : isAndroid ? 75 * formulaWidth : 75,
							top : 0 * formulaHeight,
							zIndex : 4,
							left : 0 * formulaWidth
						});

						if (dataEscenaris[i].thumb90x90 != null) {

							viewImatge.image = urlBase + dataEscenaris[i].thumb90x90;
						} else {
							viewImatge.image = '/images/imagendefectopequena.png';
						}

						row.add(viewImatge);

						var labelTitolEscenari = Ti.UI.createLabel({
							text : dataEscenaris[i].titulo,
							left : 85 * formulaWidth,
							height : Ti.UI.SIZE,
							width : 165 * formulaWidth,
							color : Ti.App.colorSecundari,

							focusedBackgroundColor : '#9c9e9f',
							bubbleParent : true,
							layout : layout,
							primera : true,
							descripcio : dataEscenaris[i].descripcio,

							font : {

								fontSize : 15 + Ti.App.paddingLabels + 'sp',
								fontFamily : customFont
							},
							top : 10 * formulaHeight
						});
						layout.add(labelTitolEscenari);

						var labelCreadorEscenari = Ti.UI.createLabel({
							text : dataEscenaris[i].creador,
							left : 85 * formulaWidth,
							height : Ti.UI.SIZE,

							width : 175 * formulaWidth,
							color : Ti.App.colorSecundari,
							bubbleParent : true,

							font : {
								fontSize : 13 + Ti.App.paddingLabels + 'sp',
								fontFamily : customFont
							},
							top : 1 * formulaHeight
						});
						layout.add(labelCreadorEscenari);

						if (dataEscenaris[i].etiquetes != null) {
							var viewEtiquetes = Ti.UI.createView({
								width : 290 * formulaWidth,
								height : Ti.UI.SIZE,
								top : 25 * formulaHeight,
								zIndex : 4,
								layout : 'horizontal',
								//backgroundImage : '/images/imagendefectopequena.png',
								left : 5 * formulaWidth
							});
							layout.add(viewEtiquetes);
							for (var j = 0; j < dataEscenaris[i].etiquetes.length; j++) {
								var labelEtiqueta = Ti.UI.createLabel({
									text : ' ' + dataEscenaris[i].etiquetes[j].etiqueta + ' ',
									left : 5 * formulaWidth,
									height : 15 * formulaWidth,

									width : Ti.UI.SIZE,
									color : 'white',
									bubbleParent : true,
									backgroundColor : '#9c9e9f',
									font : {
										fontSize : 12 + Ti.App.paddingLabels + 'sp',
										fontFamily : customFont
									},
									top : 0 * formulaHeight
								});
								viewEtiquetes.add(labelEtiqueta);

								if (labelEtiqueta.top != viewEtiquetes.top) {
									labelEtiqueta.top = 5;
								}
							}
							var viewPadding = Ti.UI.createView({
								width : 290 * formulaWidth,
								height : isAndroid ? 10 * formulaWidth : 10,
								top : 0 * formulaHeight,
								zIndex : 4,
								left : 0 * formulaWidth
							});
							layout.add(viewPadding);
						}

						arrayRows.push(row);

						var rowPadding = Ti.UI.createTableViewRow({
							height : 10 * formulaHeight,
							width : 320 * formulaWidth,
							backgroundColor : 'transparent',
							className : 'default2'

						});
						arrayRows.push(rowPadding);
					}
				}
			}
		}
		//TABLE VIEW
		if (table != null) {
			if (!isAndroid) {
				table.data = null;
			}

			viewLlistat.remove(table);

		}

		table = Ti.UI.createTableView({
			data : arrayRows,
			backgroundSelectedColor : '#9c9e9f',
			selectedBackgroundColor : '#9c9e9f',
			height : isAndroid ? 480 * formulaHeight  - 44 * formulaHeight -statusBarHeight: iOS7 ? 480 * formulaHeight - 44 - 20 : 480 * formulaHeight - 44,
			width : 320 * formulaWidth - 20 * formulaWidth,
			top : isAndroid ? 0 * formulaHeight : 0,
			left : 10 * formulaWidth,
			separatorStyle : 'none',
			backgroundColor : Ti.App.colorPrincipal
		});

		viewLlistat.add(table);
		table.addEventListener('click', function(e) {

			primeraVegada = true;

			//finestra carregant...
			var downloaderWindow = require('/controller/offline/DownloaderWindow');
			var downloaderWindowInstance = new downloaderWindow(carregant);

			// carrego dades de lescenari clicat
			var dataMHM = require('/controller/creacioEscenari/Mod_networkController');
			new dataMHM(urlBase + '/api/objectes/' + e.row.id + '/' + idioma + '_' + idiomaWeb + '/' + Ti.App.entorn, callbackMapaMHM);

			function callbackMapaMHM(data) {
				if (Ti.App.labelBarraSuperiorMapa != null) {
					Ti.App.labelBarraSuperiorMapa.text = e.row.name;
				}
				viewLlistat.animate({
					left : -320 * formulaWidth,
					duration : 500
				}, function creaMapa() {
					winCentral.idEscenari = e.row.id;

					// Creo el mapa de lescenari clicat
					var mapaMHM = require('/controller/creacioEscenari/mapController');
					var mapaMHMInstance = new mapaMHM(winCentral, data, 'llista', e.row.id);
					winCentral.add(mapaMHMInstance);
				})
			}

		});

	}

	return viewLlistat;

}

module.exports = llistat;
