function llistatDrawer(urlBase, mapaInicial, winCentral) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	if (Ti.Platform.osname == 'android') {
		var customFont = 'gotham-rounded-medium';
		var customFontBarraSuperior = 'gotham-rounded-medium';
		var customFontBold = 'gotham-rounded-medium';
	} else {
		var customFont = 'Apex New';
		var customFontBarraSuperior = 'Apex New';
		var customFontBold = 'Apex New';
	}

	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;
	var mapaMHMInstance = null;
	var tableViewProjectesInstance = null;
	var fitxaSettingsInstance = null;
	var clicat = false;
	var firstTimeLogin = true;
	var data = [];
	var titols = [{
		titol : 'Destacats',
		id : 0,
		icona : '/images/homeretos.png'
	}, {
		// titol : altres_projectes,
		titol : 'Itineraris',
		id : 1,
		icona : '/images/homerutas.png'

	}, {
		titol : 'Productors KM0',
		id : 2,
		icona : '/images/homemapa.png'
	}, {
		titol : 'Comerços',
		id : 3,
		icona : '/images/hometiendas.png'
	}, {
		titol : 'Degusta',
		id : 4,
		icona : '/images/homerestaurantes.png'
	}, {
		titol : 'Agenda',
		id : 5,
		icona : '/images/homeagenda.png'
	}];

	var viewLlistat = Ti.UI.createScrollView({
		left : 0,
		backgroundImage : '/images/fonsmenu.png',
		top : 0,
		height : isAndroid ? 480 * formulaHeight : 480 * formulaHeight,
		contentHeight : Ti.UI.SIZE,
	});

	logo = Ti.UI.createView({
		left : 0,
		backgroundImage : '/images/logomanresa.png',
		top : statusBarHeight,
		width : isAndroid ? 320 * formulaWidth : 320 * formulaWidth,
		height : isAndroid ? 44 * formulaHeight : 44 * formulaHeight,
	});
	viewLlistat.add(logo);
	var botons;
	var j = 0;
	var i = 0;
	var cont = 0;
	for (var i = 0; i < 3; i++) {
		for (var j = 0; j < 2; j++) {

			botons = Ti.UI.createView({
				left : j != 0 ? 170 * formulaWidth : 20 * formulaWidth,
				backgroundImage : titols[cont].icona,
				top : (130 * formulaHeight + 20 * formulaHeight) * i + statusBarHeight + 44 * formulaHeight,
				zIndex : 1,

				id : titols[cont].id,
				width : isAndroid ? 130 * formulaWidth : 130 * formulaWidth,
				height : isAndroid ? 130 * formulaWidth : 130 * formulaWidth,
			});

			// var icona = Ti.UI.createView({
			// left : j != 0 ? 170 * formulaWidth : 20 * formulaWidth,
			// backgroundImage : titols[i + j].icona,
			// top : (150 * i) + 44 * formulaHeight + statusBarHeight,
			// zIndex : 1,
			// width : isAndroid ? 64 * formulaWidth : 64 * formulaWidth,
			// height : isAndroid ? 64 * formulaWidth : 64 * formulaWidth,
			// });
			// botons.add(icona);

			var titolMenu = Ti.UI.createLabel({
				text : titols[cont].titol,
				left : 0 * formulaWidth,
				textAlign : 'center',
				height : Ti.UI.SIZE,
				width : isAndroid ? 130 * formulaWidth : 130 * formulaWidth,
				color : 'white',
				backgroundColor : 'transparent',
				bottom : 15 * formulaHeight,
				font : {
					fontSize : 18 + Ti.App.paddingLabels + 'sp',
					fontFamily : customFontBarraSuperior
				}
			});
			botons.add(titolMenu);

			var proximament = Ti.UI.createLabel({
				text : 'Pròximament',
				left : 0 * formulaWidth,
				textAlign : 'center',
				height : Ti.UI.SIZE,
				width : isAndroid ? 130 * formulaWidth : 130 * formulaWidth,
				color : 'white',
				backgroundColor : 'transparent',
				top : 5 * formulaHeight,
				font : {
					fontSize : 14 + Ti.App.paddingLabels + 'sp',
					fontFamily : customFontBarraSuperior
				}
			});

			if (titols[cont].id > 2) {
				botons.add(proximament);
				titolMenu.opacity = 0.4;
				botons.opacity = 0.4;
			}
			viewLlistat.add(botons);
			cont++;

			botons.addEventListener("click", function(e) {

				if (e.source.id == 0) {

					if (Ti.App.tableFiltre != null) {
						Ti.App.tableFiltre.hide();
					}

					if (mapaInicial != null) {
						winCentral.remove(mapaInicial);
					}

					if (mapaMHMInstance != null) {
						winCentral.remove(mapaMHMInstance);
						mapaMHMInstance = null;

					}
					if (tableViewProjectesInstance != null) {
						winCentral.remove(tableViewProjectesInstance);
						tableViewProjectesInstance = null;
					}

					// if (fitxaSettingsInstance != null) {
					//
					// fitxaSettingsInstance.visible = false;
					//
					// }
					// if (fitxaIdiomaInstance != null) {
					// fitxaIdiomaInstance.visible = false;
					// }

					if (!clicat) {

						clicat = true;
						if (Ti.App.labelBarraSuperiorMapa != null) {
							Ti.App.labelBarraSuperiorMapa.text = titols[0].titol;
						}

						// Mostro MHM
						// Crida HTTP per agafar les dades del escenari de MHM
						var dataMHM = require('/controller/creacioEscenari/Mod_networkController');
						new dataMHM(urlMHM, callbackMapaMHM);

						function callbackMapaMHM(data) {
							drawer.toggleLeftWindow();
							// Creo el mapa del MHM

							var mapaMHM = require('/controller/creacioEscenari/mapController');
							mapaMHMInstance = new mapaMHM(winCentral, data, 'mapa', Ti.App.idEscenariMHM);
							winCentral.add(mapaMHMInstance);
							clicat = false;
						}

					}
				} else if (e.source.id == 1) {
					if (!clicat) {
						clicat = true;
						//Mostro Projectes
						// if (filtreButton != null) {
						// filtreButton.visible = false;
						// }
						if (Ti.App.afegirPuntButton != null) {
							Ti.App.afegirPuntButton.visible = false;
						}
						createLlistatBiblioteca();
						function createLlistatBiblioteca() {

							var dataProjectes = require('controller/llistesEscenaris/llistats_network_Controller');
							var dataProjectesInstance = new dataProjectes(urlBase + '/api/scenario/library/' + idioma + '_' + idiomaWeb + '/' + Ti.App.entorn, callbackLlistatBiblioteca);

							function callbackLlistatBiblioteca(data) {
								drawer.toggleLeftWindow();

								if (Ti.App.labelBarraSuperiorMapa != null) {
									titols[e.source.id].titol
									Ti.App.labelBarraSuperiorMapa.text = titols[e.source.id].titol;
								}
								var tableViewProjectes = require('controller/llistesEscenaris/Mod_llistats');
								tableViewProjectesInstance = new tableViewProjectes(urlBase, data, 'llistat');

								winCentral.add(tableViewProjectesInstance);
								clicat = false;
							}

						}

					}
				} else if (e.source.id == 2) {
// if (filtreButton != null) {
						// filtreButton.visible = false;
						// }
					if (Ti.App.tableFiltre != null) {
						Ti.App.tableFiltre.hide();
					}

					if (mapaInicial != null) {
						winCentral.remove(mapaInicial);
					}

					if (mapaMHMInstance != null) {
						winCentral.remove(mapaMHMInstance);
						mapaMHMInstance = null;

					}
					if (tableViewProjectesInstance != null) {
						winCentral.remove(tableViewProjectesInstance);
						tableViewProjectesInstance = null;
					}

					// if (fitxaSettingsInstance != null) {
					//
					// fitxaSettingsInstance.visible = false;
					//
					// }
					// if (fitxaIdiomaInstance != null) {
					// fitxaIdiomaInstance.visible = false;
					// }

					if (!clicat) {

						clicat = true;
						if (Ti.App.labelBarraSuperiorMapa != null) {
							Ti.App.labelBarraSuperiorMapa.text = titols[e.source.id].titol;
						}

						// Mostro MHM
						// Crida HTTP per agafar les dades del escenari de MHM
						var dataMHM = require('/controller/creacioEscenari/Mod_networkController');
						new dataMHM(Ti.App.urlBase + '/api/objectes/' + '73' + '/' + idioma + '_' + idiomaWeb + '/' + Ti.App.entorn, callbackMapaMHM);

						function callbackMapaMHM(data) {
							drawer.toggleLeftWindow();
							// Creo el mapa del MHM

							var mapaMHM = require('/controller/creacioEscenari/mapController');
							mapaMHMInstance = new mapaMHM(winCentral, data, 'mapa', '73');
							winCentral.add(mapaMHMInstance);
							clicat = false;
						}

					}
				}

			});
			if (i == 2 && j == 1) {
				var padding = Ti.UI.createView({
					left : 0,
					backgroundColor : 'transparent',
					top : (130 * formulaHeight + 20 * formulaHeight) * (i + 1) + statusBarHeight + 44 * formulaHeight,
					width : isAndroid ? 320 * formulaWidth : 320 * formulaWidth,
					height : isAndroid ? 0 * formulaHeight : 0 * formulaHeight,
				});
				viewLlistat.add(padding);
			}
		}
	}
	// for (var i = 0; i < titols.length; i++) {
	// var row = Ti.UI.createTableViewRow({
	// height : isAndroid ? 44 * formulaWidth : 44,
	// top : 0,
	// left : 0,
	// color : Ti.App.colorSecundari,
	// title : '    ' + titols[i].titol,
	// backgroundSelectedColor : Ti.App.colorTerciari,
	// selectedBackgroundColor : Ti.App.colorTerciari,
	// className : 'default',
	// i : titols[i].id,
	// font : {
	// fontSize : 12 + Ti.App.paddingLabels + 'sp',
	// fontFamily : customFont
	//
	// },
	// width : isAndroid ? 320 * formulaWidth : 320,
	//
	// });
	//
	// data[i] = row;
	// }

	// var tableView = Ti.UI.createTableView({
	// separatorColor : '#000000',
	// backgroundColor : 'white',
	// backgroundSelectedColor : '#9c9e9f',
	// selectedBackgroundColor : '#9c9e9f',
	// color : 'white',
	// data : data,
	// zIndex : 1,
	// top : 0,
	// height : Ti.UI.SIZE
	// });
	// if (iOS7) {
	// tableView.top = 20;
	// }
	// tableView.addEventListener("click", function(e) {
	// if(Ti.App.tableFiltre!=null){
	// Ti.App.tableFiltre.hide();
	// }
	//
	// if (mapaInicial != null) {
	// winCentral.remove(mapaInicial);
	// }
	//
	// if (mapaMHMInstance != null) {
	// winCentral.remove(mapaMHMInstance);
	// mapaMHMInstance = null;
	//
	// }
	// if (tableViewProjectesInstance != null) {
	// winCentral.remove(tableViewProjectesInstance);
	// tableViewProjectesInstance = null;
	// }
	//
	// if (fitxaSettingsInstance != null) {
	//
	// fitxaSettingsInstance.visible = false;
	//
	// }
	// if (fitxaIdiomaInstance != null) {
	// fitxaIdiomaInstance.visible = false;
	// }
	//
	// if (e.source.i == 0) {
	// if (!clicat) {
	//
	// clicat = true;
	// if (Ti.App.labelBarraSuperiorMapa != null) {
	// Ti.App.labelBarraSuperiorMapa.text = titols[0].titol;
	// }
	//
	// // Mostro MHM
	// // Crida HTTP per agafar les dades del escenari de MHM
	// var dataMHM = require('/controller/creacioEscenari/Mod_networkController');
	// new dataMHM(urlMHM, callbackMapaMHM);
	//
	// function callbackMapaMHM(data) {
	// drawer.toggleLeftWindow();
	// // Creo el mapa del MHM
	//
	// var mapaMHM = require('/controller/creacioEscenari/mapController');
	// mapaMHMInstance = new mapaMHM(winCentral, data, 'mapa', Ti.App.idEscenariMHM);
	// winCentral.add(mapaMHMInstance);
	// clicat = false;
	// }
	//
	// }
	//
	// }
	// else if (e.source.i == 1) {
	// if (!clicat) {
	// clicat = true;
	// //Mostro Projectes
	// if (filtreButton != null) {
	// filtreButton.visible = false;
	// }
	// if (Ti.App.afegirPuntButton != null) {
	// Ti.App.afegirPuntButton.visible = false;
	// }
	// createLlistatBiblioteca();
	// function createLlistatBiblioteca() {
	//
	// var dataProjectes = require('controller/llistesEscenaris/llistats_network_Controller');
	// var dataProjectesInstance = new dataProjectes(urlBase + '/api/scenario/library/' + idioma + '_' + idiomaWeb + '/' + Ti.App.entorn, callbackLlistatBiblioteca);
	//
	// function callbackLlistatBiblioteca(data) {
	// drawer.toggleLeftWindow();
	//
	// if (Ti.App.labelBarraSuperiorMapa != null) {
	// Ti.App.labelBarraSuperiorMapa.text = altres_projectes;
	// }
	// var tableViewProjectes = require('controller/llistesEscenaris/Mod_llistats');
	// tableViewProjectesInstance = new tableViewProjectes(urlBase, data, 'llistat');
	//
	// winCentral.add(tableViewProjectesInstance);
	// clicat = false;
	// }
	//
	// }
	//
	// }
	//
	// } else if (e.source.i == 2) {
	// if (!clicat) {
	// clicat = true;
	// if (filtreButton != null) {
	// filtreButton.visible = false;
	// }
	// if (Ti.App.afegirPuntButton != null) {
	// Ti.App.afegirPuntButton.visible = false;
	// }
	//
	// if (Ti.App.labelBarraSuperiorMapa != null) {
	// Ti.App.labelBarraSuperiorMapa.text = labelIdiomaText;
	// }
	//
	// var fitxaIdioma = require('/controller/llistesEscenaris/seleccioIdioma');
	// fitxaIdiomaInstance = new fitxaIdioma();
	// winCentral.add(fitxaIdiomaInstance);
	// if (fitxaIdiomaInstance != null) {
	// fitxaIdiomaInstance.visible = true;
	// }
	// drawer.toggleLeftWindow();
	// clicat = false;
	// }
	// } else if (e.source.i == 3) {
	// if (!clicat) {
	// clicat = true;
	// if (filtreButton != null) {
	// filtreButton.visible = false;
	// }
	// if (Ti.App.afegirPuntButton != null) {
	// Ti.App.afegirPuntButton.visible = false;
	// }
	//
	// if (Ti.App.labelBarraSuperiorMapa != null) {
	// Ti.App.labelBarraSuperiorMapa.text = login;
	// }
	//
	// if (firstTimeLogin) {
	// var fitxaSettings = require('controller/llistesEscenaris/fitxaLogin');
	// fitxaSettingsInstance = new fitxaSettings(urlBase);
	// winCentral.add(fitxaSettingsInstance);
	//
	// firstTimeLogin = false;
	// }
	// if (fitxaSettingsInstance != null) {
	// fitxaSettingsInstance.visible = true;
	// }
	// drawer.toggleLeftWindow();
	// clicat = false;
	// }
	// }
	//
	// });
	//
	// viewLlistat.add(tableView);
	//
	// var viewTrencadisMHM = Ti.UI.createView({
	// left : 0,
	// backgroundColor : 'red',
	// bottom : 0,
	// width : 320 * formulaHeight,
	// height : isAndroid ? 480 * formulaHeight : 480 * formulaHeight,
	// });
	// viewLlistat.add(viewTrencadisMHM);

	// if (Ti.App.isTablet) {
	// viewTrencadisMHM.backgroundImage = '/imagesTablet/fondomenu.png';
	// }

	return viewLlistat;

}

module.exports = llistatDrawer;
