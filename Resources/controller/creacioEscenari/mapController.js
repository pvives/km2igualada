function createMapController(winCentral, data, vincDe, idEscenari) {
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}

	Ti.App.data = [];
	Ti.App.data = data.slice(0);
	if(vincDe=='mapa'){
		Ti.App.idEscenari = Ti.App.idEscenariMHM;
	}
	else if(vincDe=='mapaInicial'){
		Ti.App.idEscenari = Ti.App.idEscenariMHM;
	}
	else if(vincDe=='llista'){
		Ti.App.idEscenari = idEscenari;
	}
	
	function isiOS7Plus() {
		// iOS-specific test
		if (Titanium.Platform.name == 'iPhone OS') {
			var version = Titanium.Platform.version.split(".");
			var major = parseInt(version[0], 10);

			// Can only test this support on a 3.2+ device
			if (major >= 7) {
				return true;
			}
		}
		return false;
	}


	Ti.Geolocation.removeEventListener("location", function(e) {
	});

	// if(Ti.App.idEscenari!=Ti.App.idEscenariMHM){
	// Ti.App.labelBarraSuperiorMapa.width= isAndroid?320 * formulaWidth-44 * formulaWidth-44 * formulaWidth-44 * formulaWidth:320 * formulaWidth-44-44-44
	// }
	// else{
	// Ti.App.labelBarraSuperiorMapa.width= 320 * formulaWidth-44 * formulaWidth-44 * formulaWidth;
	// }
	var iOS7 = isiOS7Plus();
	var loggedIn = Titanium.App.Properties.getString("loggedIn");
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var statusBarHeight = isAndroid ? Ti.Platform.displayCaps.logicalDensityFactor * 25 : 20;

	var urlBase = winCentral.urlBase;

	var mapView = Ti.UI.createWebView({
		height : isAndroid ? heightpantalla - (44 * formulaHeight) - statusBarHeight : iOS7 ? heightpantalla - 44 - 20 : heightpantalla - 44,
		width : widthpantalla,
		url : "/controller/leafletmap/map.html",
		softKeyboardOnFocus : isAndroid ? Ti.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS : null,
		borderRadius : 1,
		top : iOS7 ? 20 + 44 : isAndroid ? 44 * formulaHeight : 44,
		left : 0
	});
	winCentral.add(mapView);

	if (vincDe == 'llista') {
		//si vinc de llista escurco el label de la barra superior perquè hi càpiga el botó afegir punt
		Ti.App.labelBarraSuperiorMapa.width = 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth;

	} else {
		Ti.App.labelBarraSuperiorMapa.width = 320 * formulaWidth - 44 * formulaWidth - 44 * formulaWidth;

	}
	mapView.addEventListener("load", function(e) {

		if (isAndroid) {
			//alert(Ti.Platform.displayCaps.density);
			mapView.evalJS('setupMap("android", "' + Ti.Platform.displayCaps.density + '")');
		} else {
			mapView.evalJS('setupMap("iphone", "")');
		}

		mapView.evalJS('addClusteredMarkers({data: ' + JSON.stringify(data) + '})');
		//mapView.evalJS("changeUserPosition({lat: " + 41.2 + ", lon: " + 2.18 +  ", accuracy:" + 100 + "});");
		if (isAndroid) {

			Titanium.Geolocation.Android.manualMode = false;
			Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_HIGH;
			Titanium.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;

			// mapView.evalJS("changeUserPosition({lat: " + 41.2 + ", lon: " + 2.18 + ", accuracy:" + 100 + "});");

			Titanium.Geolocation.getCurrentPosition(function(e) {
				
				if (!e.error) {
					// alert("Latitude: " + e.coords.latitude);
					var latitude = e.coords.latitude;
					var longitude = e.coords.longitude;
					var accuracy = e.coords.accuracy;
					Ti.App.currentLatitude = latitude;
					Ti.App.currentLongitude = longitude;
					Ti.App.currentAccuracy = accuracy;
					//alert(latitude + " --- " + longitude);
					mapView.evalJS("changeUserPosition({lat: " + latitude + ", lon: " + longitude + ", accuracy:" + accuracy + "});");
				}
			});
			/*
			 var gpsProvider = Ti.Geolocation.Android.createLocationProvider({
			 name: Ti.Geolocation.PROVIDER_GPS,
			 minUpdateTime: 20,
			 minUpdateDistance: 10
			 });
			 Ti.Geolocation.Android.addLocationProvider(gpsProvider);
			 */
			Ti.Geolocation.addEventListener("location", function(e) {
				if (!e.error) {
					// alert("Latitude: " + e.coords.latitude);
					var latitude = e.coords.latitude;
					var longitude = e.coords.longitude;
					var accuracy = e.coords.accuracy;
					Ti.App.currentLatitude = latitude;
					Ti.App.currentLongitude = longitude;
					Ti.App.currentAccuracy = accuracy;
					//alert(latitude + " --- " + longitude);
					mapView.evalJS("changeUserPosition({lat: " + latitude + ", lon: " + longitude + ", accuracy:" + accuracy + "});");
				}
			});
		}

		/* GEOLOCALITZACIÓ iOS
		 */
		else {
			if (!isiOS7Plus)
				Ti.Geolocation.purpose = "Mostrar posició de l'usuari al mapa.";
			Ti.Geolocation.getCurrentPosition(function(e) {
				if (Ti.Geolocation.locationServicesAuthorization == Ti.Geolocation.AUTHORIZATION_AUTHORIZED) {
					Ti.Geolocation.addEventListener("location", function(e) {
						if (!e.error) {
							var latitude = e.coords.latitude;
							var longitude = e.coords.longitude;
							var accuracy = e.coords.accuracy;
							Ti.App.currentLatitude = latitude;
							Ti.App.currentLongitude = longitude;
							Ti.App.currentAccuracy = accuracy;
							//Ti.App.fireEvent("updateLocationMarker", {lat: latitude, lon: longitude});
							//alert("latitude: " + latitude + " longitude: " + longitude);
							mapView.evalJS("changeUserPosition({lat: " + latitude + ", lon: " + longitude + ", accuracy:" + accuracy + "});");
						}
					});
				}
			});
			Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_NEAREST_TEN_METERS;
			Ti.Geolocation.activityType = Ti.Geolocation.ACTIVITYTYPE_FITNESS;
			Ti.Geolocation.distanceFilter = 5;
			if (Ti.Geolocation.locationServicesAuthorization != Ti.Geolocation.AUTHORIZATION_AUTHORIZED) {
				var alertDialog = Titanium.UI.createAlertDialog({
					title : xhr_error_title,
					message : alerta_current_location_message,
					buttonNames : ['OK']
				});
				alertDialog.show();
			}
		}
	});

	if (vincDe == 'mapaInicial') {
		Ti.App.addEventListener("openCard", function(e) {

			for (var i = 0; i < Ti.App.data.length; i++) {
				if (Ti.App.data[i].id == e.id) {

					var fitxaAportacioMHM = require('controller/objectes/puntMHM');
					var fitxaAportacioInstance = new fitxaAportacioMHM(winCentral, idEscenari, Ti.App.data[i], mapView);
					
					winCentral.add(fitxaAportacioInstance);
					
				}
			}
		});
	}
	
	// var filtre = require('/controller/creacioEscenari/filtreAnnotations');
	// var filtreInstance = new filtre(mapView, winCentral, data);

	if (vincDe == 'llista') {
		//si vinc de llista escurco el label de la barra superior perquè hi càpiga el botó afegir punt

		Ti.App.afegirPuntButton = Ti.UI.createView({
			width : 44 * formulaWidth,
			height : isAndroid ? 44 * formulaHeight : 44,
			right : 44 * formulaWidth + 5 * formulaWidth,
			top : 0 * formulaHeight,
			bubbleParent : true,
			zIndex : 2,
			backgroundImage : '/images/create.png'
		});

		// Ti.App.barraSuperior.add(Ti.App.afegirPuntButton);

		if (Ti.App.isTablet) {
			Ti.App.afegirPuntButton.backgroundImage = '/imagesTablet/create.png';
		}

		Ti.App.afegirPuntButton.addEventListener('click', function(e) {
			if (loggedIn == 'true') {
				var fitxaAportacioMHM = require('controller/objectes/creacioPunt');
				var fitxaAportacioInstance = new fitxaAportacioMHM(winCentral, 'creacioPunt', winCentral, mapView, idEscenari, data);
				
				winCentral.add(fitxaAportacioInstance);
				
			} else {
				alert(loguejat_punts);
			}
		});
	} else {
		if (Ti.App.afegirPuntButton != null) {
			Ti.App.afegirPuntButton.visible = false;
		}

	}

	return mapView;
}

module.exports = createMapController;
