function createNetworkController(postUrlPunt, postData, callback) {
	if (Titanium.App.Properties.getString("language", "undefined") == 'ca') {
		Titanium.include('/controller/translations.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'es') {
		Titanium.include('/controller/translations_es.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'en') {
		Titanium.include('/controller/translations_en.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'pt-PT') {
		Titanium.include('/controller/translations_pt.js');
	} else if (Titanium.App.Properties.getString("language", "undefined") == 'eus') {
		Titanium.include('/controller/translations_eus.js');
	} else {
		Titanium.include('/controller/translations_en.js');
	}

	var firsttime = true;
	var error = false;
	var xhr = Titanium.Network.createHTTPClient();

	// var saltedPassword = Titanium.App.Properties.getString("saltedPassword", 'null');
	// var userName = Titanium.App.Properties.getString("user",'null');

	// var wsse = require('controller/llistesEscenaris/wsse');
	// var wsseHeader = wsse.wsseHeader(userName, saltedPassword);

	xhr.onload = function() {
		
		try {
			
			if(this.responseText!='Cap objecte a mostrar'){
				var data = JSON.parse(this.responseText);
				callback(data);
			}
			else{
				alert(this.responseText);
			}
		
			//crida a createAnnotations
			
		} catch(e) {
			
			alert(e);
		}

	};

	xhr.open('POST', postUrlPunt);

	// xhr.setRequestHeader('X-WSSE', wsseHeader);
	//xhr.setRequestHeader("Content-Type", "application/json-rpc");
	xhr.send(postData);
Ti.API.info(postUrlPunt);
Ti.API.info(postData.capes);
Ti.API.info(postData.categories);
	xhr.onerror = function(e) {

		//this fires if Titanium/the native SDK cannot successfully retrieve a resource

		if (firsttime == true) {
			var alertDialog = Titanium.UI.createAlertDialog({
				title : xhr_error_title,
				message : xhr_error_message,
				buttonNames : ['ok']
			});
			alertDialog.show();
			firsttime = false;
			error = true;
		}
	};
	return error;
}

module.exports = createNetworkController;
