function createFitxaVideo(data, urlBase, i) {
	var videoPlayer;
	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;

	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var fitxaVideo = Ti.UI.createView({
		width : 300 * formulaWidth,
		height : isAndroid ? 200 * formulaHeight : 200,
		left : 10 * formulaWidth,
		zIndex : 101,
		backgroundColor : 'transparent',
		top : isAndroid ? 5 * formulaHeight : 5,
		visible : true

	});
	var actInd = Ti.UI.createActivityIndicator({
		color : 'black',
		font : {
			fontSize : 15 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFont
		},

		message : carregant,
		style : isAndroid ? 1 : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
		top : 0,
		left : 0,
		zIndex : 2,
		textAlign : 'center',
		width : 300 * formulaWidth,
		height : isAndroid ? 200 * formulaHeight : 200,
	});
	fitxaVideo.add(actInd);
	actInd.show();
	Ti.App.addEventListener("tancaVideo", function(e) {

	});
	if (data.aportacions[i].video != null && data.aportacions[i].video != '') {
		youtube_parser(data.aportacions[i].video);
	}

	function youtube_parser(url) {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		// if (match && match[7].length == 11) {
		// var webView = Ti.UI.createWebView({
		// url : 'http://www.youtube.com/embed/' + match[7] + '?autoplay=1&autohide=1&cc_load_policy=0&color=white&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0',
		// enableZoomControls : false,
		// scalesPageToFit : true,
		// zIndex : 101,
		// borderRadius : isAndroid ? 1 : 0,
		// // pluginState : isAndroid?Titanium.UI.Android.WEBVIEW_PLUGINS_ON:'0',
		// width : 300 * formulaWidth,
		// height : isAndroid ? 200 * formulaHeight : 200,
		// scrollsToTop : false,
		// showScrollbars : false
		// });
		//
		// fitxaVideo.add(webView);
		// webView.addEventListener("load", function() {
		// actInd.hide();
		// });
		// webView.addEventListener("error", function() {
		// actInd.hide();
		// });
		//
		// if (isAndroid) {
		//
		// if (Titanium.Platform.version == '4.4.2'||parseInt(Titanium.Platform.version) < 3) {
		// webView.touchEnabled = false;
		//
		// var tapaWebView = Ti.UI.createView({
		// width : 300 * formulaWidth,
		// height : isAndroid ? 200 * formulaHeight : 200,
		// left : 0 * formulaWidth,
		// zIndex : 102,
		// bubbleParent : false,
		// backgroundColor : 'transparent',
		// top : isAndroid ? 0 * formulaHeight : 0,
		// visible : true
		//
		// });
		// fitxaVideo.add(tapaWebView);
		// tapaWebView.addEventListener("click", function() {
		//
		// Titanium.Platform.openURL('http://www.youtube.com/embed/' + match[7]);
		//
		// });
		//
		// }
		// }
		//
		// } else {

		var webView = Ti.UI.createWebView({
			url : data.aportacions[i].video,
			enableZoomControls : false,
			scalesPageToFit : true,
			zIndex : 10,
			width : 300 * formulaWidth,
			height : isAndroid ? 200 * formulaHeight : 200,
			scrollsToTop : false,
			showScrollbars : false
		});

		fitxaVideo.add(webView);

		if (isAndroid) {

			if (Titanium.Platform.version == '4.4.2' || parseInt(Titanium.Platform.version) < 3) {
				webView.touchEnabled = false;

				var tapaWebView = Ti.UI.createView({
					width : 300 * formulaWidth,
					height : isAndroid ? 200 * formulaHeight : 200,
					left : 0 * formulaWidth,
					zIndex : 102,
					bubbleParent : false,
					backgroundColor : 'transparent',
					top : isAndroid ? 0 * formulaHeight : 0,
					visible : true

				});
				fitxaVideo.add(tapaWebView);
				tapaWebView.addEventListener("click", function() {

					Titanium.Platform.openURL('http://www.youtube.com/embed/' + match[7]);

				});

			}
		}

		Ti.App.addEventListener("tancaVideo", function(e) {
			if (isAndroid) {
				webView.pause();

			}
			webView.url = 'http://www.google.es';
		});

		webView.addEventListener("load", function() {
			actInd.hide();

		});
		webView.addEventListener("error", function() {
			actInd.hide();
		});
		// webView.addEventListener("click", function() {
		// Ti.Platform.openURL('http://www.youtube.com/watch?v=' + myVideoID);
		// actInd.hide();
		// });

		// }

	}

	return fitxaVideo;

}

module.exports = createFitxaVideo; 