function descripcioAportacio(dataAportacio, i, aportacioView) {
		

	var labelDescripcioTitol = Ti.UI.createLabel({
		text : descripcio,
		top : isAndroid ? 10 * formulaHeight : 10,
		left : 10 * formulaWidth,
		height : Ti.UI.SIZE,
		color : '#9c9e9f',
		textAlign : 'left',
		width : 280 * formulaWidth,
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFont
		}
	});
	aportacioView.add(labelDescripcioTitol);

	var labelDescripcio = Ti.UI.createLabel({
		clearOnEdit : false,
		zIndex : 15,
		backgroundFocusedColor : 'transparent',
		autocorrect : false,
		hintText : '+ ' + descripcio,
		returnKeyType : Titanium.UI.RETURNKEY_DEFAULT,
		keyboardType : Titanium.UI.KEYBOARD_ASCII,
		enableReturnKey : true,
		verticalAlign : Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
		editable : false,
		top : isAndroid ? 0 * formulaHeight : 0,
		left : 10 * formulaWidth,
		height : Ti.UI.SIZE,
		color : 'black',
		textAlign : 'left',
		width : 300 * formulaWidth,
		font : {
			fontSize : 12 + Ti.App.paddingLabels + 'sp',
			fontFamily : customFont
		}
	});

	aportacioView.add(labelDescripcio);


	if (dataAportacio != null) {
		if (idioma == 'ca') {
			if (dataAportacio.aportacions[i].descripciocaES != null && dataAportacio.aportacions[i].descripciocaES != '') {
				labelDescripcio.text = dataAportacio.aportacions[i].descripciocaES;
			} else {
				if (dataAportacio.aportacions[i].descripcio != null && dataAportacio.aportacions[i].descripcio != '') {
					labelDescripcio.text = dataAportacio.aportacions[i].descripcio;
				} else {
					labelDescripcioTitol.text = '';
					labelDescripcioTitol.height = 0;
					labelDescripcio.height = 0;

				}

			}
		} else if (idioma == 'es') {
			if (dataAportacio.aportacions[i].descripcioes != null && dataAportacio.aportacions[i].descripcioes != '') {
				labelDescripcio.text = dataAportacio.aportacions[i].descripcioes;
			} else {
				if (dataAportacio.aportacions[i].descripcio != null && dataAportacio.aportacions[i].descripcio != '') {
					labelDescripcio.text = dataAportacio.aportacions[i].descripcio;
				} else {
					labelDescripcioTitol.text = '';
					labelDescripcioTitol.height = 0;

				}
			}
		} else if (idioma == 'en') {
			if (dataAportacio.aportacions[i].descripcioen != null && dataAportacio.aportacions[i].descripcioen != '') {
				labelDescripcio.text = dataAportacio.aportacions[i].descripcioen;
			} else {
				if (dataAportacio.aportacions[i].descripcio != null && dataAportacio.aportacions[i].descripcio != '') {
					labelDescripcio.text = dataAportacio.aportacions[i].descripcio;
				} else {

					labelDescripcioTitol.text = '';
					labelDescripcioTitol.height = 0;

				}
			}
		} else if (idioma == 'ar') {
			if (dataAportacio.aportacions[i].descripcioar != null && dataAportacio.aportacions[i].descripcioar != '') {
				labelDescripcio.text = dataAportacio.aportacions[i].descripcioar;
			} else {
				if (dataAportacio.aportacions[i].descripcio != null && dataAportacio.aportacions[i].descripcio != '') {
					labelDescripcio.text = dataAportacio.aportacions[i].descripcio;
				} else {
					labelDescripcioTitol.text = '';
					labelDescripcioTitol.height = 0;

				}
			}
		} else {
			if (dataAportacio.aportacions[i].descripcio != null && dataAportacio.aportacions[i].descripcio != '') {
				labelDescripcio.text = dataAportacio.aportacions[i].descripcio;
			} else {
				labelDescripcioTitol.text = '';
				labelDescripcioTitol.height = 0;

			}
		}
	}

	return labelDescripcio;

}

module.exports = descripcioAportacio;