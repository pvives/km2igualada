function createFitxaAudio(data, urlBase, i) {

	//Crea les fitxes de tots els objectes
	var widthpantalla = Ti.Platform.displayCaps.platformWidth;
	var heightpantalla = Ti.Platform.displayCaps.platformHeight;
	var formulaWidth = widthpantalla / 320;
	var formulaHeight = heightpantalla / 480;
	var progressSliding = false;
	if (Titanium.Platform.osname === 'android') {
		var isAndroid = true;
	}
	var statusBarHeight = Ti.Platform.displayCaps.logicalDensityFactor * 25;

	var botoAudioView = Ti.UI.createView({
		width : 320 * formulaWidth,
		left : isAndroid ? 0 * formulaWidth : 0,
		height : isAndroid ? 44 * formulaHeight : 44 * formulaHeight ,
		top : isAndroid ? 10 * formulaHeight : 10
	});
	if (data != null) {
		if (data.aportacions[i].audio != null && data.aportacions[i].audio != '') {
			var botoAudio = Ti.UI.createButton({
				width : 320 * formulaWidth,
				// backgroundImage : '/images/playimage.png',
				color:Ti.App.colorPrincipal,
				title:reproduir_audio,
				backgroundColor:'transparent',
				left : isAndroid ? 0 * formulaWidth : 0,
				height : isAndroid ? 44 * formulaHeight : 44 * formulaHeight,
				top : isAndroid ? 0 * formulaHeight : 0
			});

			botoAudioView.add(botoAudio);

		
			var urlAudio = Ti.App.escenariDescarregat ? data.aportacions[i].audio : urlBase + '/' + data.aportacions[i].audio;

			Ti.App.audioPlayer = Ti.Media.createAudioPlayer({
				allowBackground : false,
				height : isAndroid ? 44 * formulaWidth : 44,
				url : urlAudio
			});

			// var labelNomAudio = Ti.UI.createLabel({
				// top : isAndroid ? 0 * formulaHeight : 0,
				// text : data.aportacions[i].audioorig != null ? data.aportacions[i].audioorig : '',
				// left : 30 * formulaWidth,
				// color : 'black',
				// touchEnabled : false,
				// height : isAndroid ? 25 * formulaHeight : 25,
				// textAlign : 'left',
				// width : Ti.UI.SIZE,
				// font : {
					// fontSize : 12 + Ti.App.paddingLabels + 'sp',
					// fontFamily : customFont
				// }
			// });
			// botoAudioView.add(labelNomAudio);
			// var str=data.aportacions[i].audio;
			// var res = str.split("/");
			// var audioPlayernterval = setInterval(function() {
			// if (audioPlayer.playing) {
			//
			// var audioPlayerPosition = audioPlayer.time;
			// var audioPlayerDuration = audioPlayer.duration;
			//
			// if (!isAndroid) {
			// //duration = duration * 1000;
			// }
			// if (!progressSliding) {
			// var position = audioPlayer.time, duration = audioPlayer.duration, division = 100 / duration;
			// audioPositionSlider.setValue(position * division);
			// var currentPositionValue = convertFromMsToHHMMSS(position);
			// currentPositionLabel.setText(currentPositionValue);
			// var remainingTimeValue = calculateTimeRemaining(position, duration);
			// remainingTimeLabel.setText(remainingTimeValue);
			// }
			// }
			// }, 250);
			//
			// function calculateTimeRemaining(positionInMs, totalDuration) {
			// var remainingTime = totalDuration - positionInMs;
			// var remainingFormatted = convertFromMsToHHMMSS(remainingTime);
			// var returnValue = "-" + remainingFormatted;
			// return returnValue;
			// }
			//
			// function convertFromMsToHHMMSS(miliseconds) {
			// var parsedMiliseconds = parseInt(miliseconds, 10);
			// var sec_num = parsedMiliseconds / 1000;
			// var hours = Math.floor(sec_num / 3600);
			// hours = hours.toFixed(0);
			// var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
			// minutes = minutes.toFixed(0);
			// var seconds = sec_num - (hours * 3600) - (minutes * 60);
			// seconds = seconds.toFixed(0);
			//
			// //if (hours   > 1) {hours   = "0"+hours;}
			// if (minutes < 10) {
			// minutes = "0" + minutes;
			// }
			// if (seconds < 10) {
			// seconds = "0" + seconds;
			// }
			// if (hours < 1) {
			// var time = minutes + ':' + seconds;
			// } else {
			// var time = hours + ':' + minutes + ':' + seconds;
			// }
			//
			// return time;
			// }
			//
			// var audioPositionSlider = Ti.UI.createSlider({
			// top : isAndroid ? 2 * formulaHeight : 2,
			// min : 0,
			// max : 100,
			// width : isAndroid ? 225 * formulaWidth : 225,
			// value : 0,
			// height : isAndroid ? 25 * formulaWidth : 25,
			// left : isAndroid ? 30 * formulaWidth : 30,
			// leftTrackImage : "/images/timeline.png",
			// rightTrackImage : "/images/timeline.png",
			// thumbImage : "/images/controladorrepro.png"
			// });
			// botoAudioView.add(audioPositionSlider);
			//
			// audioPositionSlider.addEventListener("touchstart", function(e) {
			// progressSliding = true;
			//
			// });
			//
			// audioPositionSlider.addEventListener("touchend", function(e) {
			// progressSliding = false;
			//
			// var duration = audioPlayer.duration;
			//
			// var division = duration / 100;
			// var currentPosition = audioPositionValue * division;
			// if (isAndroid) {
			// currentPosition = currentPosition * 1000;
			// }
			// audioPlayer.setTime(currentPosition / 1000);
			//
			// });
			//
			// if (isAndroid)
			// var audioPositionValue = 0;
			// {
			// audioPositionSlider.addEventListener("change", function(e) {
			// audioPositionValue = e.value;
			// });
			// }
			//
			// var remainingTimeLabel = Ti.UI.createLabel({
			// height : isAndroid ? 25 * formulaHeight : 25,
			// font : {
			// fontSize : 10 + Ti.App.paddingLabels + 'sp',
			// },
			// color : "black",
			// text : "0:00",
			//
			// top : isAndroid ? 0 * formulaHeight : 0,
			// right : isAndroid ? 0 * formulaWidth : 0,
			// width : isAndroid ? 44 * formulaWidth : 44,
			// textAlign : "center"
			// });
			// botoAudioView.add(remainingTimeLabel);
			//
			// var currentPositionLabel = Ti.UI.createLabel({
			// height : isAndroid ? 20 * formulaHeight : 20,
			// font : {
			// fontSize : '10dp'
			// },
			// color : "white",
			// text : "0:00",
			// height : isAndroid ? 25 * formulaWidth : 25,
			// top : isAndroid ? 10 * formulaHeight : 10,
			// left : isAndroid ? 5 * formulaWidth : 5,
			// width : isAndroid ? 44 * formulaWidth : 44,
			// textAlign : "center"
			// });
			// // botoAudioView.add(currentPositionLabel);

			botoAudio.addEventListener('click', function(e) {
				if (Ti.App.audioPlayer.playing) {
					if (botoAudio.backgroundImage == '/images/pauseimage.png' || botoAudio.backgroundImage == '/imagesTablet/pauseimage.png') {
						Ti.App.audioPlayer.pause();
						botoAudio.title = reproduir_audio;

						
					}

				} else {
					if (botoAudio.backgroundImage == '/images/playimage.png' || botoAudio.backgroundImage == '/imagesTablet/playimage.png') {
						Ti.App.audioPlayer.play();
						botoAudio.title = parar_audio;
						
					}
				}
			});

		}

	}
	return botoAudioView;

}

module.exports = createFitxaAudio;
